﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using spaar.ModLoader.UI;
using UnityEngine;

namespace spaar.ModLoader.Internal.Tools {
    // Token: 0x02000085 RID: 133
    public class InspectorPanel : MonoBehaviour {
        // Token: 0x170000A7 RID: 167
        // (get) Token: 0x060003F5 RID: 1013 RVA: 0x00011426 File Offset: 0x0000F626
        // (set) Token: 0x060003F6 RID: 1014 RVA: 0x00011430 File Offset: 0x0000F630
        public static bool autoUpdate = false;

        public GameObject SelectedGameObject {
            get {
                return this._selectedGameObject;
            }
            set {
                this._selectedGameObject = value;
                this.entries.Clear();
                if (value != null) {
                    foreach (Component component in value.GetComponents<Component>()) {
                        this.entries.Add(new ComponentEntry(component));
                    }
                }
            }
        }

        // Token: 0x170000A8 RID: 168
        // (get) Token: 0x060003F7 RID: 1015 RVA: 0x00011483 File Offset: 0x0000F683
        public bool IsGameObjectSelected {
            get {
                return this.SelectedGameObject != null;
            }
        }

        // Token: 0x060003F8 RID: 1016 RVA: 0x00011494 File Offset: 0x0000F694
        private void OnGUI() {
            if (this.activeMember != null && Event.current.keyCode == KeyCode.Return) {
                Type dataType = this.activeMember.type;
                float num5;
                if (dataType == typeof(string) || dataType == typeof(bool)) {
                    this.activeMember.SetValue(this.activeComponent, this.activeMemberNewValue);
                }
                else if (dataType == typeof(int) || dataType.IsEnum) {
                    int num;
                    if (this.activeMemberNewValue != null && int.TryParse(this.activeMemberNewValue.ToString(), out num)) {
                        this.activeMember.SetValue(this.activeComponent, num);
                    }
                }
                else if (dataType == typeof(Vector3)) {
                    Vector3 vector = (Vector3)this.activeMember.GetValue(this.activeComponent);
                    float num2;
                    if (this.activeMemberNewValue != null && float.TryParse(this.activeMemberNewValue.ToString(), out num2)) {
                        switch (this.activeMemberFieldType) {
                            case InspectorPanel.FieldType.VectorX:
                                vector.x = num2;
                                break;
                            case InspectorPanel.FieldType.VectorY:
                                vector.y = num2;
                                break;
                            case InspectorPanel.FieldType.VectorZ:
                                vector.z = num2;
                                break;
                        }
                    }
                    this.activeMember.SetValue(this.activeComponent, vector);
                }
                else if (dataType == typeof(Color)) {
                    Color color = (Color)this.activeMember.GetValue(this.activeComponent);
                    float num3;
                    if (this.activeMemberNewValue != null && float.TryParse(this.activeMemberNewValue.ToString(), out num3)) {
                        switch (this.activeMemberFieldType) {
                            case InspectorPanel.FieldType.ColorR:
                                color.r = num3;
                                break;
                            case InspectorPanel.FieldType.ColorG:
                                color.g = num3;
                                break;
                            case InspectorPanel.FieldType.ColorB:
                                color.b = num3;
                                break;
                            case InspectorPanel.FieldType.ColorA:
                                color.a = num3;
                                break;
                        }
                    }
                    this.activeMember.SetValue(this.activeComponent, color);
                }
                else if (dataType == typeof(Quaternion)) {
                    Quaternion quaternion = (Quaternion)this.activeMember.GetValue(this.activeComponent);
                    float num4;
                    if (this.activeMemberNewValue != null && float.TryParse(this.activeMemberNewValue.ToString(), out num4)) {
                        switch (this.activeMemberFieldType) {
                            case InspectorPanel.FieldType.ColorR:
                                quaternion.x = num4;
                                break;
                            case InspectorPanel.FieldType.ColorG:
                                quaternion.y = num4;
                                break;
                            case InspectorPanel.FieldType.ColorB:
                                quaternion.z = num4;
                                break;
                            case InspectorPanel.FieldType.ColorA:
                                quaternion.w = num4;
                                break;
                        }
                    }
                    this.activeMember.SetValue(this.activeComponent, quaternion);
                }
                else if (dataType == typeof(float) && this.activeMemberNewValue != null && float.TryParse(this.activeMemberNewValue.ToString(), out num5)) {
                    this.activeMember.SetValue(this.activeComponent, num5);
                }
                this.activeMember = null;
                this.activeMemberFieldType = InspectorPanel.FieldType.Normal;
                this.activeMemberNewValue = null;
            }
            int layer;
            if (this.layerTextInputActive && Event.current.keyCode == KeyCode.Return && int.TryParse(this.layerTextInputNewValue, out layer)) {
                this.SelectedGameObject.layer = layer;
                this.layerTextInputActive = false;
            }
        }

        // Token: 0x060003F9 RID: 1017 RVA: 0x00011770 File Offset: 0x0000F970
        public void Display() {
            float inspectorPanelWidth = Elements.Settings.InspectorPanelWidth;
            GUILayout.BeginHorizontal(new GUILayoutOption[]
            {
                GUILayout.Width(inspectorPanelWidth),
                GUILayout.ExpandWidth(false)
            });
            GUILayout.BeginVertical(new GUILayoutOption[0]);
            if (this.IsGameObjectSelected) {
                this.SelectedGameObject.name = GUILayout.TextField(this.SelectedGameObject.name, Elements.InputFields.ThinNoTopBotMargin, new GUILayoutOption[]
                {
                    GUILayout.Width(inspectorPanelWidth),
                    GUILayout.ExpandWidth(false)
                });
            }
            else {
                GUILayout.TextField("Select a game object in the hierarchy", Elements.InputFields.ThinNoTopBotMargin, new GUILayoutOption[]
                {
                    GUILayout.Width(inspectorPanelWidth),
                    GUILayout.ExpandWidth(false)
                });
            }
            this.inspectorScroll = GUILayout.BeginScrollView(this.inspectorScroll, new GUILayoutOption[]
            {
                GUILayout.Width(inspectorPanelWidth),
                GUILayout.ExpandWidth(false)
            });
            if (this.IsGameObjectSelected) {

                GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                if (Elements.Tools.DoCollapseArrow(this.tagExpanded, false, new GUILayoutOption[0])) {
                    this.enabledExpanded = !this.enabledExpanded;
                }
                GUILayout.Label("Enabled: " + this.SelectedGameObject.active, Elements.Labels.LogEntry, new GUILayoutOption[]
                {
                    GUILayout.ExpandWidth(false)
                });
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                if (Elements.Tools.DoCollapseArrow(this.tagExpanded, false, new GUILayoutOption[0])) {
                    this.tagExpanded = !this.tagExpanded;
                }
                GUILayout.Label("Tag: " + this.SelectedGameObject.tag, Elements.Labels.LogEntry, new GUILayoutOption[]
                {
                    GUILayout.ExpandWidth(false)
                });
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                if (Elements.Tools.DoCollapseArrow(this.layerExpanded, new GUILayoutOption[0])) {
                    this.layerExpanded = !this.layerExpanded;
                }
                GUILayout.Label("Render Layer: " + this.SelectedGameObject.layer, Elements.Labels.LogEntry, new GUILayoutOption[]
                {
                    GUILayout.ExpandWidth(false)
                });
                GUILayout.EndHorizontal();

                if (this.enabledExpanded) {
                    if (!HierarchyPanel.isCriticalObject(this.SelectedGameObject.transform) 
                        && GUILayout.Button(this.SelectedGameObject.active.ToString(), Elements.Buttons.ComponentField, new GUILayoutOption[] {
                                GUILayout.Width(60f)
                    })) {
                        this.SelectedGameObject.SetActive(!this.SelectedGameObject.active);
                    }
                }

                if (this.layerExpanded) {
                    if (this.layerTextInputActive) {
                        this.layerTextInputNewValue = GUILayout.TextField(this.layerTextInputNewValue, Elements.InputFields.ComponentField, new GUILayoutOption[0]);
                    }
                    else {
                        string a = GUILayout.TextField(this.SelectedGameObject.layer.ToString(), Elements.InputFields.ComponentField, new GUILayoutOption[0]);
                        if (a != this.SelectedGameObject.layer.ToString()) {
                            this.layerTextInputActive = true;
                            this.layerTextInputNewValue = a;
                        }
                    }
                }
            }
            GUILayout.Label("Components:", Elements.Labels.Title, new GUILayoutOption[0]);
            foreach (ComponentEntry componentEntry in new HashSet<ComponentEntry>(this.entries)) {
                if (componentEntry.Component == null) {
                    this.entries.Remove(componentEntry);
                }
                else {
                    this.DisplayComponent(componentEntry);
                }
            }
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
            GUILayout.BeginVertical(new GUILayoutOption[0]);
            //this.DisplayFilters();
            this.DisplayObjectControls();
            this.DisplayUtilityButtons();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

        // Token: 0x060003FA RID: 1018 RVA: 0x00011A68 File Offset: 0x0000FC68
        //private void DisplayFilters() {
        //    foreach (KeyValuePair<string, bool> keyValuePair in new Dictionary<string, bool>(this.filter)) {
        //        GUIStyle style = keyValuePair.Value ? Elements.Buttons.Default : Elements.Buttons.Disabled;
        //        if (GUILayout.Button(keyValuePair.Key, style, new GUILayoutOption[0])) {
        //            this.filter[keyValuePair.Key] = !this.filter[keyValuePair.Key];
        //        }
        //    }
        //}

        private void DisplayObjectControls() {
            if (!this.SelectedGameObject || this.SelectedGameObject.ToString() == "Null") return;
            GUIStyle enabledStyle = this.SelectedGameObject.active ? Elements.Buttons.Enabled : Elements.Buttons.Default;
            string enabledString = this.SelectedGameObject.active ? "Disable" : "Enable";
            if (!HierarchyPanel.isCriticalObject(this.SelectedGameObject?.transform) 
                && GUILayout.Button(enabledString, enabledStyle, new GUILayoutOption[0])) {
                this.SelectedGameObject.SetActive(!this.SelectedGameObject.active);
            }

            GUIStyle expandedStyle = this.allComponentsExpanded ? Elements.Buttons.Enabled : Elements.Buttons.Default;
            string expandedString = this.allComponentsExpanded ? "Collapse All" : "Expand All";
            if (GUILayout.Button(expandedString, expandedStyle, new GUILayoutOption[0])) {
                this.allComponentsExpanded = !this.allComponentsExpanded;

                foreach (ComponentEntry componentEntry in new HashSet<ComponentEntry>(this.entries)) {
                    componentEntry.IsExpanded = this.allComponentsExpanded;
                }
            }
        }

        // Token: 0x060003FB RID: 1019 RVA: 0x00011B18 File Offset: 0x0000FD18
        private void DisplayUtilityButtons() {
            //GUIStyle updateStyle = autoUpdate ? Elements.Buttons.Default : Elements.Buttons.Disabled;
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Focus", new GUILayoutOption[0])) {
                MouseOrbit instance = SingleInstanceFindOnly<MouseOrbit>.Instance;
                if (instance != null && this.IsGameObjectSelected) {
                    instance.target = this.SelectedGameObject.transform;
                }
            }
            if (GUILayout.Button("Select focused", new GUILayoutOption[0])) {
                MouseOrbit instance2 = SingleInstanceFindOnly<MouseOrbit>.Instance;
                if (instance2 != null && instance2.target != null) {
                    this.SelectedGameObject = instance2.target.gameObject;
                }
            }
            //if (GUILayout.Button("Destroy", new GUILayoutOption[0])) {
            //    UnityEngine.Object.Destroy(this.SelectedGameObject);
            //}
            //if (GUILayout.Button("Auto Update", updateStyle, new GUILayoutOption[0])) {
            //    autoUpdate = !autoUpdate;
            //}
        }

        // Token: 0x060003FC RID: 1020 RVA: 0x00011C08 File Offset: 0x0000FE08
        private void DisplayComponent(ComponentEntry entry) {
            GUILayout.BeginHorizontal(new GUILayoutOption[0]);
            if (Elements.Tools.DoCollapseArrow(entry.IsExpanded, entry.mappings.Count > 0, new GUILayoutOption[0])) {
                entry.IsExpanded = !entry.IsExpanded;
            }
            if (!HierarchyPanel.isCriticalObject(entry.Component.gameObject.transform) && entry.Component is Behaviour) {
                (entry.Component as Behaviour).enabled = GUILayout.Toggle((entry.Component as Behaviour).enabled, "", Elements.Toggle.Default, new GUILayoutOption[0]);
            }
            GUIStyle enabledStyle = (entry.Component is Behaviour && !(entry.Component as Behaviour).enabled) ? Elements.Labels.LogEntryDisabled : Elements.Labels.LogEntry;
            GUILayout.TextField(entry.Component.GetType().FullName, enabledStyle, new GUILayoutOption[0]);
            GUILayout.EndHorizontal();
            if (entry.IsExpanded) {
                this.showDefinedFields(entry);
            }
        }

        //Token: 0x060003FF RID: 1023 RVA: 0x000124A8 File Offset: 0x000106A8
        //public bool IsSupported(object value) {
        //    return value is string || value is bool || value is Vector3 || value is int || value is Enum || value is float || value is Color || value is Quaternion;
        //}

        private void showDefinedFields(ComponentEntry component) {
            foreach (ComponentEntry.FieldMapping data in component.mappings) {

                GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                Elements.Tools.Indent(1);
                GUILayout.BeginVertical(new GUILayoutOption[0]);

                GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                try {
                    if (Elements.Tools.DoCollapseArrow(data.expanded, data.SetValue != null || (data.type != typeof(string) && typeof(IEnumerable).IsAssignableFrom(data.type)), new GUILayoutOption[0])) {
                        data.expanded = !data.expanded;
                    }
                    GUIStyle style = new GUIStyle(Elements.Labels.LogEntry) {
                        fontStyle = FontStyle.Bold,
                        normal = { textColor = Elements.Colors.TypeText }
                    };
                    GUIStyle style2 = new GUIStyle(Elements.Labels.LogEntry) {
                        normal = { textColor = Elements.Colors.DefaultText * 0.8f }
                    };
                    string[] typeParts = data.type.ToString().Split('.');
                        GUILayout.Label(typeParts[typeParts.Length - 1], style, new GUILayoutOption[] {
                        GUILayout.ExpandWidth(false)
                    });
                    string[] nameParts = data.name.Split('.');
                    string displayName = nameParts[nameParts.Length - 1];
                    GUILayout.Label(" " + displayName + ":", Elements.Labels.LogEntry, new GUILayoutOption[] {
                        GUILayout.ExpandWidth(false)
                    });
                    GUILayout.Label(" " + ((data.GetValue(component.Component) == null) ? "null" : data.GetValue(component.Component).ToString()), style2, new GUILayoutOption[] {
                        GUILayout.ExpandWidth(false),
                        GUILayout.Width(200f)
                    });
                } catch (Exception e) {
                    Debug.LogError(e.ToString());
                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                    GUILayout.EndHorizontal();
                    continue;
                }
                
                GUILayout.EndHorizontal();

                if (data.expanded) {
                    GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                    Elements.Tools.Indent(1);
                    if (data.type == typeof(string)) {
                        this.DoInputField(component.Component, data, data.GetValue(component.Component) as string, InspectorPanel.FieldType.Normal, 0f);
                    }
                    else if (typeof(IEnumerable).IsAssignableFrom(data.type)) {
                        GUILayout.BeginVertical(new GUILayoutOption[0]);
                        foreach (object o in (data.GetValue(component.Component) as IEnumerable)) {
                            GUILayout.Label(o.ToString(), Elements.Labels.LogEntry, new GUILayoutOption[] { GUILayout.ExpandWidth(false) });
                        }
                        GUILayout.EndVertical();
                    }
                    else if (data.type == typeof(bool)) {
                        if (GUILayout.Button(data.GetValue(component.Component).ToString(), Elements.Buttons.ComponentField, new GUILayoutOption[]
                        {
                                GUILayout.Width(60f)
                        })) {
                            data.SetValue(component.Component, !(bool)data.GetValue(component.Component));
                        }
                    }
                    else if (data.type == typeof(int) || data.type.IsEnum) {
                        this.DoInputField(component.Component, data, (int)data.GetValue(component.Component), InspectorPanel.FieldType.Normal, 0f);
                    }
                    else if (data.type == typeof(float)) {
                        this.DoInputField(component.Component, data, (float)data.GetValue(component.Component), InspectorPanel.FieldType.Normal, 0f);
                    }
                    else if (data.type == typeof(Vector3)) {
                        Vector3 vector = (Vector3)data.GetValue(component.Component);
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("X: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, vector.x, InspectorPanel.FieldType.VectorX, 80f);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("Y: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, vector.y, InspectorPanel.FieldType.VectorY, 80f);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("Z: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, vector.z, InspectorPanel.FieldType.VectorZ, 80f);
                        GUILayout.EndHorizontal();
                    }
                    else if (data.type == typeof(Color)) {
                        Color color = (Color)data.GetValue(component.Component);
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("R: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, color.r, InspectorPanel.FieldType.ColorR, 80f);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("G: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, color.g, InspectorPanel.FieldType.ColorG, 80f);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("B: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, color.b, InspectorPanel.FieldType.ColorB, 80f);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("A: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                         GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, color.a, InspectorPanel.FieldType.ColorA, 80f);
                        GUILayout.EndHorizontal();
                    }
                    else if (data.type == typeof(Quaternion)) {
                        Quaternion quaternion = (Quaternion)data.GetValue(component.Component);
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("X: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, quaternion.x, InspectorPanel.FieldType.ColorR, 80f);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("Y: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, quaternion.y, InspectorPanel.FieldType.ColorG, 80f);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("Z: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, quaternion.z, InspectorPanel.FieldType.ColorB, 80f);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                        GUILayout.Label("W: ", Elements.Labels.LogEntry, new GUILayoutOption[]
                        {
                                GUILayout.ExpandWidth(false)
                        });
                        this.DoInputField(component.Component, data, quaternion.w, InspectorPanel.FieldType.ColorA, 80f);
                        GUILayout.EndHorizontal();
                    } 
                    GUILayout.EndHorizontal();
                }

                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }
        }

        // Token: 0x060003FE RID: 1022 RVA: 0x000123CC File Offset: 0x000105CC
        private void DoInputField(Component component, ComponentEntry.FieldMapping data, object value, InspectorPanel.FieldType type, float width = 0f) {
            GUILayoutOption guilayoutOption = (width > 0f) ? GUILayout.Width(width) : GUILayout.ExpandWidth(true);

            if (this.activeMember == data && this.activeMemberFieldType == type) {
                GUI.SetNextControlName(FIELD_EDIT_INPUT_NAME + data.ID);
                this.activeMemberNewValue = GUILayout.TextField((string)this.activeMemberNewValue, Elements.InputFields.ComponentField, new GUILayoutOption[]
                {
                    guilayoutOption
                });
                return;
            }

            string text = value.ToString();
            GUI.SetNextControlName(FIELD_EDIT_INPUT_NAME + data.ID);
            string b = GUILayout.TextField(text.ToString(), Elements.InputFields.ComponentField, new GUILayoutOption[]
            {
                guilayoutOption
            });
            if (text != b) {
                this.activeComponent = component;
                this.activeMember = data;
                this.activeMemberFieldType = type;
                this.activeMemberNewValue = b;
            }
        }

        // Token: 0x040001F4 RID: 500
        private const string FIELD_EDIT_INPUT_NAME = "field_edit_input";

        // Token: 0x040001F5 RID: 501
        private ComponentEntry.FieldMapping activeMember;
        private Component activeComponent;

        // Token: 0x040001F6 RID: 502
        private InspectorPanel.FieldType activeMemberFieldType;

        // Token: 0x040001F7 RID: 503
        private object activeMemberNewValue;

        // Token: 0x040001F8 RID: 504
        private bool layerTextInputActive;

        // Token: 0x040001F9 RID: 505
        private string layerTextInputNewValue;

        // Token: 0x040001FA RID: 506
        private GameObject _selectedGameObject;

        // Token: 0x040001FB RID: 507
        //private Dictionary<string, bool> filter = new Dictionary<string, bool>
        //{
        //    {
        //        "Instance",
        //        true
        //    },
        //    {
        //        "Static",
        //        false
        //    },
        //    {
        //        "Public",
        //        true
        //    },
        //    {
        //        "NonPublic",
        //        false
        //    },
        //    {
        //        "Inherited",
        //        false
        //    },
        //    {
        //        "Has Setter",
        //        true
        //    }
        //};

        // Token: 0x040001FC RID: 508
        private readonly HashSet<ComponentEntry> entries = new HashSet<ComponentEntry>();

        // Token: 0x040001FD RID: 509
        private Vector2 inspectorScroll = Vector2.zero;

        private bool enabledExpanded;

        // Token: 0x040001FE RID: 510
        private bool tagExpanded;

        // Token: 0x040001FF RID: 511
        private bool layerExpanded;

        private bool allComponentsExpanded = true;

        // Token: 0x020000E1 RID: 225
        private enum FieldType {
            // Token: 0x04000308 RID: 776
            Normal,
            // Token: 0x04000309 RID: 777
            VectorX,
            // Token: 0x0400030A RID: 778
            VectorY,
            // Token: 0x0400030B RID: 779
            VectorZ,
            // Token: 0x0400030C RID: 780
            ColorR,
            // Token: 0x0400030D RID: 781
            ColorG,
            // Token: 0x0400030E RID: 782
            ColorB,
            // Token: 0x0400030F RID: 783
            ColorA
        }
    }
}
