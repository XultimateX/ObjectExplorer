﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace spaar.ModLoader.Internal.Tools {
    // Token: 0x02000081 RID: 129
    public class ComponentEntry {

        public class FieldMapping {
            internal bool expanded = false;
            internal string name;
            internal Type type;
            internal Func<Component, object> GetValue;
            internal Action<Component, object> SetValue;

            internal static int nextID = 0;
            internal readonly int ID = nextID++;

            public FieldMapping(string name, Type type, Func<Component, object> getValue, Action<Component, object> setValue) {
                this.name = name;
                this.type = type;
                this.GetValue = getValue;
                this.SetValue = setValue;
            }
        }

        class ComponentMapping {
            internal Type type;
            internal Dictionary<string, List<FieldMapping>> fields;

            internal ComponentMapping(Type type) {
                this.type = type;
                this.fields = new Dictionary<string, List<FieldMapping>>();
            }
        }

        static List<ComponentMapping> componentMap = new List<ComponentMapping>();

        // Token: 0x040001D6 RID: 470
        public readonly Component Component;
        // Token: 0x040001D7 RID: 471
        public bool IsExpanded = true;
        public List<FieldMapping> mappings = new List<FieldMapping>();

        public static void AddFieldMapping(Type componentType, string set, string name, Type fieldType, Func<Component, object> getValue, Action<Component, object> setValue) {
            ComponentMapping toAddTo = null;
            foreach (ComponentMapping c in componentMap) {
                if (c.type.Equals(componentType)) {
                    toAddTo = c;
                    break;
                }
            }
            if (toAddTo == null) {
                componentMap.Add(toAddTo = new ComponentMapping(componentType));
            }

            if (!toAddTo.fields.ContainsKey(set)) {
                toAddTo.fields.Add(set, new List<FieldMapping>());
            }
            toAddTo.fields[set].Add(new FieldMapping(name, fieldType, getValue, setValue));
        }

        public static void RemoveFieldMapping(Type componentType, string set, string name) {
            foreach (ComponentMapping c in componentMap) {
                if (c.type.Equals(componentType)) {
                    if (c.fields.ContainsKey(set)) {
                        List<FieldMapping> toCheck = c.fields[set];
                        FieldMapping toRemove = null;
                        foreach (FieldMapping m in toCheck) {
                            if (m.name.Equals(name)) toRemove = m;
                            break;
                        }
                        toCheck.Remove(toRemove);
                    }
                    break;
                }
            }
        }

        public static void AddComponentMappings(Type componentType, string set, List<FieldMapping> mappings) {
            ComponentMapping toAddTo = null;
            foreach (ComponentMapping c in componentMap) {
                if (c.type.Equals(componentType)) {
                    toAddTo = c;
                    break;
                }
            }
            if (toAddTo == null) {
                componentMap.Add(toAddTo = new ComponentMapping(componentType));
            }

            if (!toAddTo.fields.ContainsKey(set)) {
                toAddTo.fields.Add(set, new List<FieldMapping>());
            }
            toAddTo.fields[set].AddRange(mappings);
        }

        public static void RemoveComponentMappings(Type componentType, string set) {
            ComponentMapping toCheck = null;
            foreach (ComponentMapping c in componentMap) {
                if (c.type.Equals(componentType)) {
                    toCheck = c;
                    break;
                }
            }
            if (toCheck != null) {
                toCheck.fields.Remove(set);
            }
        }

        // Token: 0x060003CD RID: 973 RVA: 0x000102B0 File Offset: 0x0000E4B0
        public ComponentEntry(Component component) {
            this.Component = component;

            foreach (ComponentMapping m in componentMap) {
                if (component.GetType().IsAssignableFrom(m.type)) {
                    m.fields.Values.ToList().ForEach(mappings.AddRange);
                }
            }

            this.IsExpanded = mappings.Count > 0;
        }
    }
}
