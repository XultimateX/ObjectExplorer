﻿using UnityEngine;
using Modding;
using System;
using Localisation;
using Modding.Blocks;
using System.Collections;

public class AccelerometerBehaviour : MonoBehaviour {

    Vector3 lastPosition = Vector3.zero;
    DynamicText accelerationText = null;
    GameObject machine = null;
    GameObject speedometer = null;
    bool didCheck = false;
    float acceleration = 0.01f;
    float speed = 0f;
    float lastSpeed = 0f;

    // Use this for initialization
    void Start() {
        UnityEngine.Object.DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update() {
        if (speedometer == null) {
            if (didCheck) {
                return;
            }
            else {
                speedometer = GameObject.Find("Speedometer");
                didCheck = true;

            }
        }
        if (accelerationText == null || accelerationText.ToString().Equals(null)) {
            SpawnAccelerometer();
        }
        else if (Modding.Blocks.PlayerMachine.GetLocal() != null && Modding.Blocks.PlayerMachine.GetLocal().ToString() != "Null") {
            RunAccelerometer();
        }
    }

    void SpawnAccelerometer() {
        try {
            GameObject gameObject = GameObject.Find("AlignTopLeftNoFold").transform.FindChild("MenuBG").gameObject;
            GameObject gameObject2 = GameObject.Find("ServerHealth").transform.FindChild("Text").FindChild("FPS").gameObject;
            GameObject gameObject3 = GameObject.Find("ServerHealth").transform.FindChild("Text").FindChild("r_FPS_value").gameObject;
            GameObject gameObject5 = UnityEngine.Object.Instantiate<GameObject>(gameObject);
            GameObject accelerationLabel = UnityEngine.Object.Instantiate<GameObject>(gameObject2);
            GameObject accelerationTextObject = UnityEngine.Object.Instantiate<GameObject>(gameObject3);
            accelerationText = accelerationTextObject.GetComponent<DynamicText>();
            this.transform.parent = GameObject.Find("AlignTopLeftNoFold").transform;
            this.transform.position = GameObject.Find("AlignTopLeftNoFold").transform.position + Vector3.down * 0.3f;
            gameObject5.name = "CounterBG2";
            gameObject5.transform.parent = this.transform;
            gameObject5.transform.localPosition = new Vector3(0.8888893f, -0.8999996f, 2.009741f);
            gameObject5.transform.localScale = new Vector3(4.22f, 0.3f, 1f);
            gameObject5.AddComponent<BoxCollider>();
            gameObject5.GetComponent<BoxCollider>().size = new Vector3(1f, 1f, 1f);
            accelerationLabel.name = "MPS2";
            accelerationLabel.transform.parent = this.transform;
            accelerationLabel.transform.localPosition = new Vector3(1.088889f, -0.9549999f, 2.009741f);
            accelerationLabel.GetComponent<DynamicText>().size = 0.16f;
            accelerationLabel.GetComponent<DynamicText>().SetText("METRES PER SECOND ^2");
            accelerationLabel.GetComponent<DynamicText>().serializedText = "METRES PER SECOND ^2";
            UnityEngine.Object.Destroy(accelerationLabel.GetComponent<LocalisationChild>());
            accelerationTextObject.name = "MPS2_value";
            accelerationTextObject.transform.parent = this.transform;
            accelerationTextObject.transform.localPosition = new Vector3(0.4888897f, -0.9549999f, 2.009741f);
            accelerationTextObject.GetComponent<DynamicText>().size = 0.16f;
            accelerationTextObject.GetComponent<DynamicText>().SetText("57.2");
            accelerationTextObject.GetComponent<DynamicText>().serializedText = "57.2";
            didCheck = false;
        }
        catch (Exception ex) {
            Debug.LogError(string.Format("[Accelerometer] {0}.", ex.Message));
        }
    }

    void RunAccelerometer() {
        if (PlayerMachine.GetLocal() == null || PlayerMachine.GetLocal().InternalObject == null ||
            !PlayerMachine.GetLocal().InternalObject.isSimulating) return;
        //if (machine == null || machine.ToString().Equals("Null")) {
            if (StatMaster.isMP) {
                try {
                    if (Game.IsSimulating && !Game.IsSpectator) {
                        machine = PlayerMachine.GetLocal().SimulationMachine.FindChild("StartingBlock").gameObject;
                    }
                }
                catch (Exception ex) {
                    Debug.LogError(string.Format("[Accelerometer] {0}", ex.Message));
                }
            }
            else {
                machine = GameObject.Find("StartingBlock");
            }
       // }
        Vector3 velocityVector = (machine.transform.position - this.lastPosition) / Time.deltaTime;
        speed = velocityVector.magnitude;
        float actualAcceleration = (speed - lastSpeed) / Time.deltaTime;
        acceleration = (actualAcceleration == 0f || float.IsNaN(acceleration)) ? 0.01f : Mathf.Lerp(acceleration, actualAcceleration, 0.1f);
        this.lastPosition = machine.transform.position;
        this.lastSpeed = speed;

        accelerationText.SetText(
            accelerationText.serializedText = PlayerMachine.GetLocal().InternalObject.isSimulating ? acceleration.ToString("0.0") : "0.0"
        );
    }
}
