﻿using System;
using System.Linq;
using System.Collections.Generic;
using spaar.ModLoader.Internal.Tools;
using UnityEngine;

// Token: 0x02000002 RID: 2
[ExecuteInEditMode]
public class OpenFogVolume : MonoBehaviour {

    bool Initialise() {
        Resources.FindObjectsOfTypeAll<Light>().ToList().ForEach(
            x => {
                if (x.gameObject.name.Equals("Directional light Fog")) this.Sun = x;
            });
        if (!this.Sun) {
            Debug.Log("OpenFogVolume: Did not find sun light!");
            this.enabled = false;
            return false;
        }
        Renderer thisRenderer = this.gameObject.GetComponent<Renderer>();

        this.FogColor = thisRenderer.materials[0].color;
        this.InscatteringColor = thisRenderer.materials[0].GetColor("_InscatteringColor");

        FogVolume fogVolume = gameObject.GetComponent<FogVolume>();
        if (fogVolume) {
            this.Visibility = fogVolume.Visibility;
            this.InscatteringExponent = fogVolume.InscateringExponent;
            this.InscatteringIntensity = fogVolume.InscatteringIntensity;
            this.InscatteringStartDistance = fogVolume.InscatteringStartDistance;
            this.InscatteringTransitionWideness = fogVolume.InscatteringTransitionWideness;
            this._3DNoiseScale = fogVolume._3DNoiseScale;
            this._3DNoiseStepSize = fogVolume._3DNoiseStepSize;
            this.fogStartDistance = fogVolume.fogStartDistance;
            this._NoiseVolume = fogVolume._NoiseVolume;
            this.Quality = (QualityLevel)fogVolume.Quality;
            this.NoiseIntensity = fogVolume.NoiseIntensity;
            this.NoiseContrast = fogVolume.NoiseContrast;
            this.Speed = fogVolume.Speed;
            this.Stretch = fogVolume.Stretch;
            fogVolume.enabled = false;
        } else {
            FogVolumeInscatterX2 fogVolumeI2 = gameObject.GetComponent<FogVolumeInscatterX2>();
            if (fogVolumeI2) {
                this.Visibility = fogVolumeI2.Visibility;
                this.InscatteringExponent = fogVolumeI2.InscateringExponent;
                this.InscatteringIntensity = fogVolumeI2.InscatteringIntensity;
                this.InscatteringStartDistance = fogVolumeI2.InscatteringStartDistance;
                this.InscatteringTransitionWideness = fogVolumeI2.InscatteringTransitionWideness;
                this._3DNoiseScale = fogVolumeI2._3DNoiseScale;
                this._3DNoiseStepSize = fogVolumeI2._3DNoiseStepSize;
                this.fogStartDistance = fogVolumeI2.fogStartDistance;
                this._NoiseVolume = fogVolumeI2._NoiseVolume;
                this.Quality = (QualityLevel)fogVolumeI2.Quality;
                this.NoiseIntensity = fogVolumeI2.NoiseIntensity;
                this.NoiseContrast = fogVolumeI2.NoiseContrast;
                this.Speed = fogVolumeI2.Speed;
                this.Stretch = fogVolumeI2.Stretch;
                doubleInscattering = true;
                fogVolumeI2.enabled = false;
            }
        }
        this.FogMaterial = this.gameObject.GetComponent<Renderer>().materials[0];
        Debug.Log("OpenFogVolume: Initialised.");
        return true;
    }

    // Token: 0x06000003 RID: 3 RVA: 0x000021DC File Offset: 0x000003DC
    public void OnEnable() {
        if (initialised < 2) return;
        this.ToggleKeyword();
        if (!StatMaster.isHeadless) {
            Camera.main.depthTextureMode |= DepthTextureMode.Depth;
        }
    }

    // Token: 0x06000004 RID: 4 RVA: 0x00002270 File Offset: 0x00000470
    public static void Wireframe(GameObject obj, bool Enable) {
    }

    public void Awake() {
        if (mapped) return;
        List<ComponentEntry.FieldMapping> fogMappings = new List<ComponentEntry.FieldMapping>();
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.Status", typeof(int), c => (c as OpenFogVolume).initialised, null));
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.Color", typeof(Color), (c) => (c as OpenFogVolume).FogColor, 
            (c, x) => {
                (c as OpenFogVolume).FogColor = (Color)x;
                (c as OpenFogVolume).OnEnable();
            }));
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.FogStartDistance", typeof(float), (c) => (c as OpenFogVolume).fogStartDistance, 
            (c, x) => {
                (c as OpenFogVolume).fogStartDistance = (float)x;
                (c as OpenFogVolume).OnEnable();
            }));
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.Visibility", typeof(float), (c) => (c as OpenFogVolume).Visibility, 
            (c, x) => {
                (c as OpenFogVolume).Visibility = (float)x;
                (c as OpenFogVolume).OnEnable();
            }));

        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringEnabled", typeof(bool), (c) => (c as OpenFogVolume).EnableInscattering, 
            (c, x) => {
                (c as OpenFogVolume).EnableInscattering = (bool)x;
                (c as OpenFogVolume).OnEnable();
            }));
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringColor", typeof(Color), (c) => (c as OpenFogVolume).InscatteringColor, 
            (c, x) => {
                (c as OpenFogVolume).InscatteringColor = (Color)x;
                (c as OpenFogVolume).OnEnable();
            }));
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringTransitionWideness", typeof(float), (c) => (c as OpenFogVolume).InscatteringTransitionWideness, 
            (c, x) => {
                (c as OpenFogVolume).InscatteringTransitionWideness = (float)x;
                (c as OpenFogVolume).OnEnable();
            }));
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringStartDistance", typeof(float), (c) => (c as OpenFogVolume).InscatteringStartDistance, 
            (c, x) => {
                (c as OpenFogVolume).InscatteringStartDistance = (float)x;
                (c as OpenFogVolume).OnEnable();
            }));
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringIntensity", typeof(float), (c) => (c as OpenFogVolume).InscatteringIntensity, 
            (c, x) => {
                (c as OpenFogVolume).InscatteringIntensity = (float)x;
                (c as OpenFogVolume).OnEnable();
            }));
        fogMappings.Add(new ComponentEntry.FieldMapping("OpenFogVolume.InscatteringExponent", typeof(float), (c) => (c as OpenFogVolume).InscatteringExponent, 
            (c, x) => {
                (c as OpenFogVolume).InscatteringExponent = (float)x;
                (c as OpenFogVolume).OnEnable();
            }));
        ComponentEntry.AddComponentMappings(typeof(OpenFogVolume), "Atmospherics", fogMappings);
        mapped = true;

        //data.Add("OpenFogVolume.NoiseEnabled", new ComponentData(typeof(bool), () => EnableNoise, x => { EnableNoise = (bool)x; OnEnable();  return null; }));
        //data.Add("OpenFogVolume.Quality", new ComponentData(typeof(OpenFogVolume.QualityLevel), () => Quality, x => { Quality = (OpenFogVolume.QualityLevel)x; OnEnable(); return null; }));
        //data.Add("OpenFogVolume.NoiseContrast", new ComponentData(typeof(float), () => NoiseContrast, x => { NoiseContrast = (float)x; OnEnable();  return null; }));
        //data.Add("OpenFogVolume.NoiseIntensity", new ComponentData(typeof(float), () => NoiseIntensity, x => { NoiseIntensity = (float)x; OnEnable();  return null; }));
        //data.Add("OpenFogVolume.3DNoiseScale", new ComponentData(typeof(float), () => _3DNoiseScale, x => { _3DNoiseScale = (float)x; OnEnable(); return null; }));
        //data.Add("OpenFogVolume.3DNoiseStep", new ComponentData(typeof(float), () => _3DNoiseStepSize, x => { _3DNoiseStepSize = (float)x; OnEnable(); return null; }));
        //data.Add("OpenFogVolume.Speed", new ComponentData(typeof(Vector4), () => Speed));
        //data.Add("OpenFogVolume.Stretch", new ComponentData(typeof(Vector4), () => Stretch));
    }

    // Token: 0x06000005 RID: 5 RVA: 0x00002274 File Offset: 0x00000474
    private void Update() {
        if (!this.gameObject.activeInHierarchy || !this.gameObject.activeSelf) return;
        switch (initialised) {
            case 0: initialised++; break;
            case 1:
                Initialise();
                initialised++;
                OnEnable();
                break;
        }
    }

    // Token: 0x06000006 RID: 6 RVA: 0x00002278 File Offset: 0x00000478
    private void OnWillRenderObject() {
        if (initialised < 2) return;
        this.FogMaterial = this.gameObject.GetComponent<Renderer>().materials[0];
        this.FogMaterial.SetColor("_Color", this.FogColor);
        this.FogMaterial.SetColor("_InscatteringColor", this.InscatteringColor);
        this.FogMaterial.SetFloat("FogStartDistance", this.fogStartDistance);
        if (this.Sun) {
            this.FogMaterial.SetFloat("_InscatteringIntensity", this.InscatteringIntensity);
            this.FogMaterial.SetVector("L", -this.Sun.transform.forward);
            this.FogMaterial.SetFloat("_InscateringExponent", this.InscatteringExponent);
            this.FogMaterial.SetFloat("InscatteringTransitionWideness", this.InscatteringTransitionWideness);
        }
        if (this.EnableNoise && this._NoiseVolume) {
            Shader.SetGlobalTexture("_NoiseVolume", this._NoiseVolume);
            this.FogMaterial.SetFloat("gain", this.NoiseIntensity);
            this.FogMaterial.SetFloat("threshold", this.NoiseContrast * 0.5f);
            this.FogMaterial.SetFloat("_3DNoiseScale", this._3DNoiseScale * 0.001f);
            this.FogMaterial.SetFloat("_3DNoiseStepSize", this._3DNoiseStepSize * 0.001f / (float)this.Quality);
            this.FogMaterial.SetVector("Speed", this.Speed);
            this.FogMaterial.SetVector("Stretch", new Vector4(1f, 1f, 1f, 1f) + this.Stretch * 0.01f);
        }
        this.FogMaterial.SetFloat("InscatteringStartDistance", this.InscatteringStartDistance);
        Vector3 localScale = this.gameObject.transform.localScale;
        base.transform.localScale = new Vector3((float)decimal.Round((decimal)localScale.x, 2), localScale.y, localScale.z);
        this.FogMaterial.SetVector("_BoxMin", localScale * -0.5f);
        this.FogMaterial.SetVector("_BoxMax", localScale * 0.5f);
        this.FogMaterial.SetFloat("_Visibility", this.Visibility);
    }

    // Token: 0x06000007 RID: 7 RVA: 0x000024F8 File Offset: 0x000006F8
    private void ToggleKeyword() {
        if (this.EnableNoise) {
            this.FogMaterial.EnableKeyword("_FOG_VOLUME_NOISE");
        }
        else {
            this.FogMaterial.DisableKeyword("_FOG_VOLUME_NOISE");
        }
        if (this.EnableInscattering && this.Sun) {
            this.FogMaterial.EnableKeyword("_FOG_VOLUME_INSCATTERING");
        }
        else {
            this.FogMaterial.DisableKeyword("_FOG_VOLUME_INSCATTERING");
        }
        switch (this.Quality) {
            case QualityLevel.Low:
                this.FogMaterial.EnableKeyword("_LQ");
                this.FogMaterial.DisableKeyword("_MQ");
                this.FogMaterial.DisableKeyword("_HQ");
                break;
            case QualityLevel.Medium:
                this.FogMaterial.EnableKeyword("_MQ");
                this.FogMaterial.DisableKeyword("_LQ");
                this.FogMaterial.DisableKeyword("_HQ");
                break;
            case QualityLevel.High:
                this.FogMaterial.EnableKeyword("_HQ");
                this.FogMaterial.DisableKeyword("_MQ");
                this.FogMaterial.DisableKeyword("_LQ");
                break;
        }
    }

    public enum QualityLevel {
        Low = 1, Medium = 2, High = 3
    }

    // Token: 0x04000002 RID: 2
    [HideInInspector]
    public Material FogMaterial;

    // Token: 0x04000003 RID: 3
    [SerializeField]
    public Color InscatteringColor = Color.white;

    // Token: 0x04000004 RID: 4
    [SerializeField]
    public Color FogColor = new Color(0.5f, 0.6f, 0.7f, 1f);

    // Token: 0x04000011 RID: 17
    [SerializeField]
    public Light Sun;

    // Token: 0x04000012 RID: 18
    [SerializeField]
    public int DrawOrder;

    // Token: 0x04000005 RID: 5
    public float Visibility = 5f;

    // Token: 0x04000006 RID: 6
    public float InscatteringExponent = 15f;

    // Token: 0x04000007 RID: 7
    public float InscatteringIntensity = 2f;

    // Token: 0x04000008 RID: 8
    public float InscatteringStartDistance = 400f;

    // Token: 0x04000009 RID: 9
    public float InscatteringTransitionWideness = 1f;

    // Token: 0x0400000A RID: 10
    public float _3DNoiseScale = 300f;

    // Token: 0x0400000B RID: 11
    public float _3DNoiseStepSize = 50f;

    // Token: 0x0400000C RID: 12
    public float fogStartDistance = 40f;

    // Token: 0x0400000D RID: 13
    public Texture3D _NoiseVolume;

    // Token: 0x0400000E RID: 14
    [Range(1f, 3f)]
    public QualityLevel Quality = QualityLevel.Low;

    // Token: 0x0400000F RID: 15
    [Range(0f, 10f)]
    public float NoiseIntensity = 1f;

    // Token: 0x04000010 RID: 16
    [Range(0f, 1f)]
    public float NoiseContrast;

    // Token: 0x04000013 RID: 19
    [SerializeField]
    public bool EnableInscattering = true;

    // Token: 0x04000014 RID: 20
    [SerializeField]
    public bool EnableNoise = false;

    // Token: 0x04000015 RID: 21
    public Vector4 Speed = new Vector4(0f, 0f, 0f, 0f);

    // Token: 0x04000016 RID: 22
    public Vector4 Stretch = new Vector4(0f, 0f, 0f, 0f);

    public bool doubleInscattering { get; private set; } = false;

    int initialised = 0;

    static bool mapped = false;
}
