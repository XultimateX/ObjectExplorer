using System;
using Modding;
using UnityEngine;

namespace ObjectExplorer
{
	public class Mod : ModEntryPoint
	{
		public override void OnLoad()
		{
            SingleInstance<spaar.ModLoader.Internal.Tools.ObjectExplorer>.Initialize();
            UnityEngine.Object.DontDestroyOnLoad(SingleInstance<spaar.ModLoader.Internal.Tools.ObjectExplorer>.Instance);
            new GameObject("Bootleg Accelerometer").AddComponent<AccelerometerBehaviour>();
            new GameObject("Skybox Behaviour").AddComponent<SkyboxBehaviour>();
		}
	}
}
