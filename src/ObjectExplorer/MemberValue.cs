﻿//using System;
//using System.Reflection;
//using UnityEngine;

//namespace spaar.ModLoader.Internal.Tools {
//    // Token: 0x02000089 RID: 137
//    public class MemberValue {
//        // Token: 0x170000AC RID: 172
//        // (get) Token: 0x0600040F RID: 1039 RVA: 0x00012DA0 File Offset: 0x00010FA0
//        public Type Type {
//            get {
//                if (this.entryType != EntryType.Field) {
//                    return this.PropertyInfo.PropertyType;
//                }
//                return this.FieldInfo.FieldType;
//            }
//        }

//        // Token: 0x170000AD RID: 173
//        // (get) Token: 0x06000410 RID: 1040 RVA: 0x00012DC1 File Offset: 0x00010FC1
//        public bool IsObsolete {
//            get {
//                return Attribute.IsDefined(this.info, typeof(ObsoleteAttribute));
//            }
//        }

//        // Token: 0x170000AE RID: 174
//        // (get) Token: 0x06000411 RID: 1041 RVA: 0x00012DD8 File Offset: 0x00010FD8
//        public string Name {
//            get {
//                return this.info.Name;
//            }
//        }

//        // Token: 0x170000AF RID: 175
//        // (get) Token: 0x06000412 RID: 1042 RVA: 0x00012DE5 File Offset: 0x00010FE5
//        // (set) Token: 0x06000413 RID: 1043 RVA: 0x00012DED File Offset: 0x00010FED
//        public bool IsExpanded { get; set; }

//        // Token: 0x170000B0 RID: 176
//        // (get) Token: 0x06000414 RID: 1044 RVA: 0x00012DF6 File Offset: 0x00010FF6
//        // (set) Token: 0x06000415 RID: 1045 RVA: 0x00012DFE File Offset: 0x00010FFE
//        public object Dummy { get; set; }

//        // Token: 0x170000B1 RID: 177
//        // (get) Token: 0x06000416 RID: 1046 RVA: 0x00012E07 File Offset: 0x00011007
//        public bool IsPublic {
//            get {
//                if (this.entryType == EntryType.Field) {
//                    return this.FieldInfo.IsPublic;
//                }
//                return this.PropertyInfo.GetGetMethod(true).IsPublic;
//            }
//        }

//        // Token: 0x170000B2 RID: 178
//        // (get) Token: 0x06000417 RID: 1047 RVA: 0x00012E2E File Offset: 0x0001102E
//        public bool IsStatic {
//            get {
//                if (this.entryType == EntryType.Field) {
//                    return this.FieldInfo.IsStatic;
//                }
//                return this.PropertyInfo.GetGetMethod(true).IsStatic;
//            }
//        }

//        // Token: 0x170000B3 RID: 179
//        // (get) Token: 0x06000418 RID: 1048 RVA: 0x00012E55 File Offset: 0x00011055
//        // (set) Token: 0x06000419 RID: 1049 RVA: 0x00012E5D File Offset: 0x0001105D
//        public bool IsInherited { get; private set; }

//        // Token: 0x170000B4 RID: 180
//        // (get) Token: 0x0600041A RID: 1050 RVA: 0x00012E66 File Offset: 0x00011066
//        public bool HasSetter {
//            get {
//                return this.entryType == EntryType.Field || this.PropertyInfo.GetSetMethod(true) != null;
//            }
//        }

//        // Token: 0x170000B5 RID: 181
//        // (get) Token: 0x0600041B RID: 1051 RVA: 0x00012E81 File Offset: 0x00011081
//        private FieldInfo FieldInfo {
//            get {
//                return this.info as FieldInfo;
//            }
//        }

//        // Token: 0x170000B6 RID: 182
//        // (get) Token: 0x0600041C RID: 1052 RVA: 0x00012E8E File Offset: 0x0001108E
//        private PropertyInfo PropertyInfo {
//            get {
//                return this.info as PropertyInfo;
//            }
//        }

//        // Token: 0x0600041D RID: 1053 RVA: 0x00012E9B File Offset: 0x0001109B
//        public MemberValue(Component component, PropertyInfo property, bool inherited) : this(component, EntryType.Property, property, inherited) {
//        }

//        // Token: 0x0600041E RID: 1054 RVA: 0x00012EA7 File Offset: 0x000110A7
//        public MemberValue(Component component, FieldInfo field, bool inherited) : this(component, EntryType.Field, field, inherited) {
//        }

//        // Token: 0x0600041F RID: 1055 RVA: 0x00012EB4 File Offset: 0x000110B4
//        private MemberValue(Component component, EntryType entryType, MemberInfo info, bool inherited) {
//            this.component = component;
//            this.entryType = entryType;
//            this.info = info;
//            this.Dummy = this.GetValue();
//            this.IsInherited = inherited;
//        }

//        // Token: 0x06000420 RID: 1056 RVA: 0x00012F03 File Offset: 0x00011103
//        public void SetValue(object value) {
//            this.Dummy = value;
//            if (this.entryType == EntryType.Field) {
//                this.FieldInfo.SetValue(this.component, value);
//                return;
//            }
//            this.PropertyInfo.SetValue(this.component, value, null);
//        }

//        // Token: 0x06000421 RID: 1057 RVA: 0x00012F3A File Offset: 0x0001113A
//        public object GetValue() {
//            if (this.entryType != EntryType.Field) {
//                return this.PropertyInfo.GetValue(this.component, null);
//            }
//            return this.FieldInfo.GetValue(this.component);
//        }

//        // Token: 0x04000213 RID: 531
//        private static int nextID;

//        // Token: 0x04000217 RID: 535
//        public readonly int ID = MemberValue.nextID++;

//        // Token: 0x04000218 RID: 536
//        private readonly EntryType entryType;

//        // Token: 0x04000219 RID: 537
//        private readonly Component component;

//        // Token: 0x0400021A RID: 538
//        private readonly MemberInfo info;
//    }
//}
