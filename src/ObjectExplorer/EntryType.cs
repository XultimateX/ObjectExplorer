﻿using System;

namespace spaar.ModLoader.Internal.Tools {
    // Token: 0x02000088 RID: 136
    internal enum EntryType {
        // Token: 0x04000211 RID: 529
        Field,
        // Token: 0x04000212 RID: 530
        Property
    }
}
