﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using spaar.ModLoader.UI;
using UnityEngine;
using Modding;

namespace spaar.ModLoader.Internal.Tools {
    // Token: 0x0200008B RID: 139
    public class ObjectExplorer : SingleInstance<ObjectExplorer> {
        // Token: 0x170000B8 RID: 184
        // (get) Token: 0x0600042A RID: 1066 RVA: 0x00013421 File Offset: 0x00011621
        public override string Name {
            get {
                return "spaar's Mod Loader: Object Explorer";
            }
        }

        // Token: 0x170000B9 RID: 185
        // (get) Token: 0x0600042B RID: 1067 RVA: 0x00013428 File Offset: 0x00011628
        // (set) Token: 0x0600042C RID: 1068 RVA: 0x00013430 File Offset: 0x00011630
        public HierarchyPanel HierarchyPanel { get; private set; }

        // Token: 0x170000BA RID: 186
        // (get) Token: 0x0600042D RID: 1069 RVA: 0x00013439 File Offset: 0x00011639
        // (set) Token: 0x0600042E RID: 1070 RVA: 0x00013441 File Offset: 0x00011641
        public InspectorPanel InspectorPanel { get; private set; }

        // Token: 0x0600042F RID: 1071 RVA: 0x0001344C File Offset: 0x0001164C
        private void Start() {
            this.HierarchyPanel = base.gameObject.AddComponent<HierarchyPanel>();
            this.InspectorPanel = base.gameObject.AddComponent<InspectorPanel>();
            this.key = ModKeys.GetKey("object-explorer");

            List<ComponentEntry.FieldMapping> mappingsList = new List<ComponentEntry.FieldMapping>();
            mappingsList.Add(new ComponentEntry.FieldMapping("Transform.Position", typeof(Vector3), c => (c as Transform).position, (c, x) => (c as Transform).position = (Vector3)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Transform.Rotation", typeof(Quaternion), c => (c as Transform).rotation, (c, x) => (c as Transform).rotation = (Quaternion)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Transform.EulerAngles", typeof(Vector3), c => (c as Transform).eulerAngles, (c, x) => (c as Transform).eulerAngles = (Vector3)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Transform.LocalPosition", typeof(Vector3), c => (c as Transform).localPosition, (c, x) => (c as Transform).localPosition = (Vector3)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Transform.LocalRotation", typeof(Quaternion), c => (c as Transform).localRotation, (c, x) => (c as Transform).localRotation = (Quaternion)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Transform.LocalEulerAngles", typeof(Vector3), c => (c as Transform).localEulerAngles, (c, x) => (c as Transform).localEulerAngles = (Vector3)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Transform.LocalScale", typeof(Vector3), c => (c as Transform).localScale, (c, x) => (c as Transform).localScale = (Vector3)x));
            ComponentEntry.AddComponentMappings(typeof(Transform), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.Flares", typeof(Dictionary<string, Cubemap>), (c) => SkyboxBehaviour.lensFlares.Keys, null));
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.CurrentFlare", typeof(string), (c) => (c as Light).flare ? (c as Light).flare.name : "null", 
                (c, x) => {
                    if ((x as string).Equals("null")) {
                        (c as Light).flare = null;
                        return;
                    }
                    foreach (string s in SkyboxBehaviour.lensFlares.Keys) {
                        if (s.StartsWith(x as string)) {
                            (c as Light).flare = SkyboxBehaviour.lensFlares[x as string];
                            break;
                        }
                    }
                }));
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.RenderMode", typeof(LightRenderMode), (c) => (c as Light).renderMode, (c, x) => (c as Light).renderMode = (LightRenderMode)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.Range", typeof(float), (c) => (c as Light).range, (c, x) => (c as Light).range = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.Colour", typeof(Color), (c) => (c as Light).color, (c, x) => (c as Light).color = (Color)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.Intensity", typeof(float), (c) => (c as Light).intensity, (c, x) => (c as Light).intensity = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.ShadowStrength", typeof(float), (c) => (c as Light).shadowStrength, (c, x) => (c as Light).shadowStrength = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.AmbientMode", typeof(UnityEngine.Rendering.AmbientMode), (c) => RenderSettings.ambientMode, (c, x) => RenderSettings.ambientMode = (UnityEngine.Rendering.AmbientMode)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Light.AmbientColour", typeof(Color), (c) => RenderSettings.ambientLight, (c, x) => RenderSettings.ambientLight = (Color)x));
            ComponentEntry.AddComponentMappings(typeof(Light), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("Renderer.Materials", typeof(Material[]), (c) => (c as Renderer).materials, null));
            mappingsList.Add(new ComponentEntry.FieldMapping("Renderer.Color", typeof(Color), (c) => (c as Renderer).materials[0]?.color, (c, x) => (c as Renderer).materials[0]?.SetColor("_Color", (Color)x)));
            ComponentEntry.AddComponentMappings(typeof(Renderer), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("GlobalFog.FogMode", typeof(FogMode), (c) => (c as GlobalFog).fogMode, (c, x) => (c as GlobalFog).fogMode = (GlobalFog.FogMode)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("GlobalFog.Color", typeof(Color), (c) => (c as GlobalFog).globalFogColor, (c, x) => (c as GlobalFog).globalFogColor = (Color)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("GlobalFog.StartDistance", typeof(float), (c) => (c as GlobalFog).startDistance, (c, x) => (c as GlobalFog).startDistance = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("GlobalFog.GlobalDensity", typeof(float), (c) => (c as GlobalFog).globalDensity, (c, x) => (c as GlobalFog).globalDensity = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("GlobalFog.Height", typeof(float), (c) => (c as GlobalFog).height, (c, x) => (c as GlobalFog).height = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("GlobalFog.HeightScale", typeof(float), (c) => (c as GlobalFog).heightScale, (c, x) => (c as GlobalFog).heightScale = (float)x));
            ComponentEntry.AddComponentMappings(typeof(GlobalFog), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("ColorfulFog.FogStart", typeof(float), c => (c as ColorfulFog).fogStart, (c, x) => (c as ColorfulFog).fogStart = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("ColorfulFog.FogEnd", typeof(float), c => (c as ColorfulFog).fogEnd, (c, x) => (c as ColorfulFog).fogEnd = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("ColorfulFog.FogDensity", typeof(float), c => (c as ColorfulFog).fogDensity, (c, x) => (c as ColorfulFog).fogDensity = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("ColorfulFog.StartDistance", typeof(float), c => (c as ColorfulFog).startDistance, (c, x) => (c as ColorfulFog).startDistance = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("ColorfulFog.HeightDensity", typeof(float), c => (c as ColorfulFog).heightDensity, (c, x) => (c as ColorfulFog).heightDensity = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("ColorfulFog.Height", typeof(float), c => (c as ColorfulFog).height, (c, x) => (c as ColorfulFog).height = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("ColorfulFog.HeightFog", typeof(bool), c => (c as ColorfulFog).heightFog, (c, x) => (c as ColorfulFog).heightFog = (bool)x));
            ComponentEntry.AddComponentMappings(typeof(ColorfulFog), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("Camera.MinRenderDistance", typeof(float), c => (c as Camera).nearClipPlane, (c, x) => (c as Camera).nearClipPlane = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Camera.MaxRenderDistance", typeof(float), c => (c as Camera).farClipPlane, (c, x) => (c as Camera).farClipPlane = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Camera.FOV", typeof(float), c => (c as Camera).fieldOfView, (c, x) => (c as Camera).fieldOfView = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Camera.Orthographic", typeof(bool), c => (c as Camera).orthographic, (c, x) => (c as Camera).orthographic = (bool)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Camera.OrthographicSize", typeof(float), c => (c as Camera).orthographicSize, (c, x) => (c as Camera).orthographicSize = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Camera.BackgroundColor", typeof(Color), c => (c as Camera).backgroundColor, (c, x) => (c as Camera).backgroundColor = (Color)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Camera.ShadowDistance", typeof(float), c => QualitySettings.shadowDistance, (c, x) => QualitySettings.shadowDistance = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Camera.ClearFlags", typeof(CameraClearFlags), c => (c as Camera).clearFlags, null));
            ComponentEntry.AddComponentMappings(typeof(Camera), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("MouseOrbit.WASDMovement", typeof(bool), c => MouseOrbit.AllowWASDCamControl, (c, x) => MouseOrbit.AllowWASDCamControl = (bool)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("MouseOrbit.MaxZoom", typeof(float), c => MouseOrbit.maxZoom, (c, x) => MouseOrbit.maxZoom = (float)x));
            ComponentEntry.AddComponentMappings(typeof(MouseOrbit), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("Rigidbody.Drag", typeof(float), c => (c as Rigidbody).drag, (c, x) => (c as Rigidbody).drag = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Rigidbody.AngularDrag", typeof(float), c => (c as Rigidbody).angularDrag, (c, x) => (c as Rigidbody).angularDrag = (float)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("Rigidbody.Mass", typeof(float), c => (c as Rigidbody).mass, (c, x) => (c as Rigidbody).mass = (float)x));
            ComponentEntry.AddComponentMappings(typeof(Rigidbody), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("Collider.IsTrigger", typeof(bool), c => (c as Collider).isTrigger, null));
            mappingsList.Add(new ComponentEntry.FieldMapping("Collider.StaticFriction", typeof(float), c => (c as Collider)?.material?.staticFriction, null));
            mappingsList.Add(new ComponentEntry.FieldMapping("Collider.DynamicFriction", typeof(float), c => (c as Collider)?.material?.dynamicFriction, null));
            mappingsList.Add(new ComponentEntry.FieldMapping("Collider.Bounciness", typeof(float), c => (c as Collider)?.material?.bounciness, null));
            mappingsList.Add(new ComponentEntry.FieldMapping("Collider.FrictionCombine", typeof(PhysicMaterialCombine), c => (c as Collider)?.material?.frictionCombine, null));
            mappingsList.Add(new ComponentEntry.FieldMapping("Collider.BounceCombine", typeof(PhysicMaterialCombine), c => (c as Collider)?.material?.bounceCombine, null));
            ComponentEntry.AddComponentMappings(typeof(Collider), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("AxialDrag.AxisDrag", typeof(Vector3), c => (c as AxialDrag).AxisDrag, (c, x) => (c as AxialDrag).AxisDrag = (Vector3)x));
            ComponentEntry.AddComponentMappings(typeof(AxialDrag), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("ParticleSystem.InheritVelocity", typeof(bool), c => (c as ParticleSystem).inheritVelocity.enabled, 
                (c, x) => {
                    ParticleSystem.InheritVelocityModule module = (c as ParticleSystem).inheritVelocity;
                    module.enabled = (bool)x;
                }));
            mappingsList.Add(new ComponentEntry.FieldMapping("ParticleSystem.ParticleDrag", typeof(bool), c => (c as ParticleSystem).limitVelocityOverLifetime.enabled, 
                (c, x) => {
                    ParticleSystem.LimitVelocityOverLifetimeModule module = (c as ParticleSystem).limitVelocityOverLifetime;
                    module.enabled = (bool)x;
                }));
            mappingsList.Add(new ComponentEntry.FieldMapping("ParticleSystem.ParticleDragForce", typeof(float), c => (c as ParticleSystem).limitVelocityOverLifetime.dampen, 
                (c, x) => {
                    ParticleSystem.LimitVelocityOverLifetimeModule module = (c as ParticleSystem).limitVelocityOverLifetime;
                    module.dampen = (float)x;
                }));
            mappingsList.Add(new ComponentEntry.FieldMapping("ParticleSystem.ParticleDragThresholdStart", typeof(bool), c => (c as ParticleSystem).limitVelocityOverLifetime.limit.constantMax, null));
            mappingsList.Add(new ComponentEntry.FieldMapping("ParticleSystem.ParticleDragThresholdEnd", typeof(bool), c => (c as ParticleSystem).limitVelocityOverLifetime.limit.constantMin, null));
            ComponentEntry.AddComponentMappings(typeof(ParticleSystem), "ObjectExplorer", mappingsList);

            mappingsList.Clear();
            mappingsList.Add(new ComponentEntry.FieldMapping("ParticleAddForce.AddForceSelf", typeof(bool), c => (c as ParticleAddForce).addForceToOwnBlock, (c, x) => (c as ParticleAddForce).addForceToOwnBlock = (bool)x));
            mappingsList.Add(new ComponentEntry.FieldMapping("ParticleAddForce.UseRelativeForce", typeof(bool), c => (c as ParticleAddForce).useRelativeForce, (c, x) => (c as ParticleAddForce).useRelativeForce = (bool)x));
            ComponentEntry.AddComponentMappings(typeof(ParticleAddForce), "ObjectExplorer", mappingsList);

        }

        // Token: 0x06000430 RID: 1072 RVA: 0x0001349D File Offset: 0x0001169D
        private void Update() {
            if (this.key.IsPressed) {
                this.IsVisible = !this.IsVisible;
            }

            if (!canary || canary.ToString() == "Null") {
                canary = new GameObject();
                Resources.FindObjectsOfTypeAll<FogVolume>().ToList().ForEach(x => {
                    if (x.hideFlags == HideFlags.None && !x.GetComponent<OpenFogVolume>()) x.gameObject.AddComponent<OpenFogVolume>();
                });
                Resources.FindObjectsOfTypeAll<FogVolumeInscatterX2>().ToList().ForEach(x => {
                    if (x.hideFlags == HideFlags.None && !x.GetComponent<OpenFogVolume>()) x.gameObject.AddComponent<OpenFogVolume>();
                });
            }
        }

        // Token: 0x06000431 RID: 1073 RVA: 0x000134BC File Offset: 0x000116BC
        private void OnGUI() {
            GUI.skin = ModGUI.Skin;
            if (this.IsVisible) {
                this.windowRect = GUILayout.Window(this.windowID, this.windowRect, new GUI.WindowFunction(this.DoWindow), this.WindowTitle, new GUILayoutOption[0]);
                //this.windowRect = Util.PreventOffScreenWindow(this.windowRect);
            }
        }

        // Token: 0x06000432 RID: 1074 RVA: 0x0001351C File Offset: 0x0001171C
        private void DoWindow(int id) {
            GUILayout.BeginHorizontal(new GUILayoutOption[0]);
            this.HierarchyPanel.Display();
            this.InspectorPanel.Display();
            GUILayout.EndHorizontal();
            GUI.DragWindow(new Rect(0f, 0f, this.windowRect.width, (float)GUI.skin.window.padding.top));
        }

        // Token: 0x04000226 RID: 550
        public string WindowTitle = "Object Explorer";

        // Token: 0x04000229 RID: 553
        private readonly int windowID = 0; //Util.GetWindowID();

        // Token: 0x0400022A RID: 554
        private Rect windowRect = new Rect(20f, 20f, 800f, 600f);

        // Token: 0x0400022B RID: 555
        private ModKey key;

        // Token: 0x0400022C RID: 556
        public bool IsVisible;

        private GameObject canary;
    }
}
