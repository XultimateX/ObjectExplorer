﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace spaar.ModLoader.Internal.Tools {
    // Token: 0x02000083 RID: 131
    public class HierarchyEntry {
        // Token: 0x170000A5 RID: 165
        // (get) Token: 0x060003DF RID: 991 RVA: 0x00010C16 File Offset: 0x0000EE16
        public bool HasChildren {
            get {
                return this.Children.Count > 0;
            }
        }

        // Token: 0x170000A6 RID: 166
        // (get) Token: 0x060003E0 RID: 992 RVA: 0x00010C26 File Offset: 0x0000EE26
        // (set) Token: 0x060003E1 RID: 993 RVA: 0x00010C2E File Offset: 0x0000EE2E
        public bool IsExpanded { get; set; }

        // Token: 0x060003E2 RID: 994 RVA: 0x00010C37 File Offset: 0x0000EE37
        public HierarchyEntry(Transform transform) {
            this.Transform = transform;
            this.Children = new HashSet<HierarchyEntry>();
            this.UpdateChildrenList();
        }

        // Token: 0x060003E3 RID: 995 RVA: 0x00010C58 File Offset: 0x0000EE58
        private void UpdateChildrenList() {
            this.Children.Clear();
            foreach (object obj in this.Transform) {
                Transform transform = (Transform)obj;
                this.Children.Add(new HierarchyEntry(transform));
            }
        }

        // Token: 0x060003E4 RID: 996 RVA: 0x00010CC8 File Offset: 0x0000EEC8
        public override int GetHashCode() {
            return this.Transform.GetHashCode();
        }

        // Token: 0x040001E9 RID: 489
        public readonly Transform Transform;

        // Token: 0x040001EA RID: 490
        public HashSet<HierarchyEntry> Children;
    }
}
