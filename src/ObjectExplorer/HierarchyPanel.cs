﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using spaar.ModLoader.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace spaar.ModLoader.Internal.Tools {
    // Token: 0x02000084 RID: 132
    public class HierarchyPanel : MonoBehaviour {
        // Token: 0x060003E5 RID: 997 RVA: 0x00010CD8 File Offset: 0x0000EED8
        private void Start() {
            this.RefreshGameObjectList();
            this.shouldUpdate = InspectorPanel.autoUpdate;
            base.StartCoroutine(this.AutoUpdate());
            //Configuration.OnConfigurationChange += this.OnConfigurationChange;
            SceneManager.sceneLoaded += this.OnSceneLoaded;
        }

        // Token: 0x060003E6 RID: 998 RVA: 0x00010D2B File Offset: 0x0000EF2B
        private void OnConfigurationChange(object s, object e) {
            bool flag = this.shouldUpdate;
            this.shouldUpdate = InspectorPanel.autoUpdate; //Configuration.GetBool("objExpAutoUpdate", false);
            if (!flag) {
                base.StartCoroutine(this.AutoUpdate());
            }
        }

        // Token: 0x060003E7 RID: 999 RVA: 0x00010D53 File Offset: 0x0000EF53
        private IEnumerator AutoUpdate() {
            while (this.shouldUpdate) {
                yield return new WaitForSeconds(0.5f);
                if (SingleInstance<ObjectExplorer>.Instance.IsVisible) {
                    this.RefreshGameObjectList();
                }
            }
            yield break;
        }

        // Token: 0x060003E8 RID: 1000 RVA: 0x00010D62 File Offset: 0x0000EF62
        private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
            this.RefreshGameObjectList();
        }

        // Token: 0x060003E9 RID: 1001 RVA: 0x00010D6C File Offset: 0x0000EF6C
        public void Display() {
            GUILayout.BeginVertical(new GUILayoutOption[0]);
            GUILayout.BeginHorizontal(new GUILayoutOption[]
            {
                GUILayout.Width(Elements.Settings.HierarchyPanelWidth)
            });
            GUI.SetNextControlName("search_field");
            string text = this.searchFieldText;
            this.searchFieldText = GUILayout.TextField(text, Elements.InputFields.ThinNoTopBotMargin, new GUILayoutOption[]
            {
                GUILayout.Width(160f)
            });
            if (text != this.searchFieldText) {
                this.RefreshSearchList();
            }
            bool flag = this.AreAllCollapsed(this.inspectorEntries);
            if (GUILayout.Button(flag ? "Expand All" : "Collapse All", Elements.Buttons.ThinNoTopBotMargin, new GUILayoutOption[]
            {
                GUILayout.Width(90f)
            })) {
                if (flag) {
                    this.ExpandAll(this.inspectorEntries);
                }
                else {
                    this.CollapseAll(this.inspectorEntries);
                }
            }
            if (GUILayout.Button("Refresh", Elements.Buttons.ThinNoTopBotMargin, new GUILayoutOption[0])) {
                this.RefreshGameObjectList();
            }
            if (Event.current.type == EventType.Repaint) {
                if (GUI.GetNameOfFocusedControl() == "search_field") {
                    if (this.searchFieldText == "Search") {
                        this.isSearching = true;
                        this.searchFieldText = "";
                        this.RefreshSearchList();
                    }
                }
                else if (this.searchFieldText.Length < 1) {
                    this.isSearching = false;
                    this.searchFieldText = "Search";
                }
            }
            GUILayout.EndHorizontal();
            this.hierarchyScroll = GUILayout.BeginScrollView(this.hierarchyScroll, false, true, new GUILayoutOption[]
            {
                GUILayout.Width(Elements.Settings.HierarchyPanelWidth)
            });
            this.DoShowEntries(this.inspectorEntries, 0);
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        // Token: 0x060003EA RID: 1002 RVA: 0x00010F20 File Offset: 0x0000F120
        private bool AreAllCollapsed(IEnumerable<HierarchyEntry> entries) {
            foreach (HierarchyEntry hierarchyEntry in entries) {
                if (hierarchyEntry.IsExpanded) {
                    return false;
                }
                if (hierarchyEntry.HasChildren && !this.AreAllCollapsed(hierarchyEntry.Children)) {
                    return false;
                }
            }
            return true;
        }

        // Token: 0x060003EB RID: 1003 RVA: 0x00010F8C File Offset: 0x0000F18C
        private void ExpandAll(IEnumerable<HierarchyEntry> entries) {
            foreach (HierarchyEntry hierarchyEntry in entries) {
                hierarchyEntry.IsExpanded = true;
                if (hierarchyEntry.HasChildren) {
                    this.ExpandAll(hierarchyEntry.Children);
                }
            }
        }

        // Token: 0x060003EC RID: 1004 RVA: 0x00010FE8 File Offset: 0x0000F1E8
        private void CollapseAll(IEnumerable<HierarchyEntry> entries) {
            foreach (HierarchyEntry hierarchyEntry in entries) {
                hierarchyEntry.IsExpanded = false;
                if (hierarchyEntry.HasChildren) {
                    this.CollapseAll(hierarchyEntry.Children);
                }
            }
        }

        // Token: 0x060003ED RID: 1005 RVA: 0x00011044 File Offset: 0x0000F244
        private void DoShowEntries(IEnumerable<HierarchyEntry> entries, int iterationDepth = 0) {
            foreach (HierarchyEntry hierarchyEntry in entries) {
                if (!(hierarchyEntry.Transform == null) && (!this.isSearching || this.searchFilteredEntries.Contains(hierarchyEntry))) {
                    GUILayout.BeginHorizontal(new GUILayoutOption[0]);
                    Elements.Tools.Indent(iterationDepth);

                    if (Elements.Tools.DoCollapseArrow(hierarchyEntry.IsExpanded && hierarchyEntry.HasChildren, hierarchyEntry.HasChildren, new GUILayoutOption[0])) {
                        hierarchyEntry.IsExpanded = !hierarchyEntry.IsExpanded;
                    }

                    GUIStyle selected;
                    if (hierarchyEntry.Transform.gameObject.active) {
                        selected = (SingleInstance<ObjectExplorer>.Instance.InspectorPanel.SelectedGameObject == hierarchyEntry.Transform.gameObject)
                            ? Elements.Buttons.LogEntryLabelSelected : Elements.Buttons.LogEntryLabel;
                    } else {
                        selected = (SingleInstance<ObjectExplorer>.Instance.InspectorPanel.SelectedGameObject == hierarchyEntry.Transform.gameObject)
                            ? Elements.Buttons.LogEntryLabelSelectedDisabled : Elements.Buttons.LogEntryLabelDisabled;
                    }

                    if (!isCriticalObject(transform) && GUILayout.Button(hierarchyEntry.Transform.name, selected, new GUILayoutOption[0])) {
                        SingleInstance<ObjectExplorer>.Instance.InspectorPanel.SelectedGameObject = hierarchyEntry.Transform.gameObject;
                    }
                    GUILayout.EndHorizontal();
                    if (hierarchyEntry.IsExpanded) {
                        this.DoShowEntries(hierarchyEntry.Children, iterationDepth + 1);
                    }
                }
            }
        }

        // Token: 0x060003EE RID: 1006 RVA: 0x0001115C File Offset: 0x0000F35C
        public void RefreshGameObjectList() {
            HashSet<HierarchyEntry> hashSet = new HashSet<HierarchyEntry>();
            foreach (Transform transform in UnityEngine.Object.FindObjectsOfType<Transform>()) {
                if (transform.parent == null && !transform.name.Equals("spaar's Mod Loader: Object Explorer")) {
                    HierarchyEntry item = new HierarchyEntry(transform);
                    hashSet.Add(item);
                }
            }
            this.CopyIsExpanded(this.inspectorEntries, hashSet);
            this.inspectorEntries = hashSet;
            if (this.isSearching) {
                this.RefreshSearchList();
            }
        }

        // Token: 0x060003EF RID: 1007 RVA: 0x000111C8 File Offset: 0x0000F3C8
        private void CopyIsExpanded(HashSet<HierarchyEntry> src, HashSet<HierarchyEntry> dest) {
            using (HashSet<HierarchyEntry>.Enumerator enumerator = src.GetEnumerator()) {
                while (enumerator.MoveNext()) {
                    HierarchyEntry entry = enumerator.Current;
                    HierarchyEntry hierarchyEntry;
                    if ((hierarchyEntry = dest.FirstOrDefault((HierarchyEntry e) => e.Transform == entry.Transform)) != null) {
                        hierarchyEntry.IsExpanded = entry.IsExpanded;
                        this.CopyIsExpanded(entry.Children, hierarchyEntry.Children);
                    }
                }
            }
        }

        // Token: 0x060003F0 RID: 1008 RVA: 0x0001125C File Offset: 0x0000F45C
        private void RefreshSearchList() {
            this.searchFilteredEntries.Clear();
            foreach (HierarchyEntry root in this.inspectorEntries) {
                this.searchFilteredEntries.UnionWith(this.Flatten(root));
            }
            this.searchFilteredEntries.RemoveWhere((HierarchyEntry e) => !this.EntryOrChildrenContain(e, this.searchFieldText));
        }

        // Token: 0x060003F1 RID: 1009 RVA: 0x000112E0 File Offset: 0x0000F4E0
        private HashSet<HierarchyEntry> Flatten(HierarchyEntry root) {
            HashSet<HierarchyEntry> hashSet = new HashSet<HierarchyEntry>
            {
                root
            };
            HashSet<HierarchyEntry> children = root.Children;
            if (children != null) {
                foreach (HierarchyEntry root2 in children) {
                    hashSet.UnionWith(this.Flatten(root2));
                }
            }
            return hashSet;
        }

        // Token: 0x060003F2 RID: 1010 RVA: 0x00011350 File Offset: 0x0000F550
        private bool EntryOrChildrenContain(HierarchyEntry entry, string text) {
            string value = text.ToLower();
            if (entry.Transform == null) {
                return false;
            }
            if (entry.Transform.name.ToLower().Contains(value)) {
                return true;
            }
            foreach (HierarchyEntry entry2 in entry.Children) {
                if (this.EntryOrChildrenContain(entry2, text)) {
                    return true;
                }
            }
            return false;
        }

        public static bool isCriticalObject(Transform toCheck) {
            if (toCheck == null || toCheck.Equals(null)) return false;
            return toCheck.root.name.Equals("_PERSISTENT") || toCheck.root.name.Equals("NETWORKING");
        }

        // Token: 0x040001EC RID: 492
        private const string SEARCH_FIELD_NAME = "search_field";

        // Token: 0x040001ED RID: 493
        private const string SEARCH_FIELD_DEFAULT = "Search";

        // Token: 0x040001EE RID: 494
        private HashSet<HierarchyEntry> inspectorEntries = new HashSet<HierarchyEntry>();

        // Token: 0x040001EF RID: 495
        private HashSet<HierarchyEntry> searchFilteredEntries = new HashSet<HierarchyEntry>();

        // Token: 0x040001F0 RID: 496
        private Vector2 hierarchyScroll = Vector2.zero;

        // Token: 0x040001F1 RID: 497
        private string searchFieldText = "Search";

        // Token: 0x040001F2 RID: 498
        private bool isSearching;

        // Token: 0x040001F3 RID: 499
        private bool shouldUpdate = false;
    }
}
