﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Modding;
using spaar.ModLoader.Internal.Tools;

public class SkyboxBehaviour : MonoBehaviour {

    public static Dictionary<String, Flare> lensFlares = null;

    public Dictionary<string, Cubemap> skyboxTextures = new Dictionary<string, Cubemap>();
    Material skyMaterial;
    public string currentKey = "null";
    static bool mapped = false;

    void Awake() {
        if (mapped) return;
        List<ComponentEntry.FieldMapping> skyboxMappings = new List<ComponentEntry.FieldMapping>();
        skyboxMappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.Skyboxes", typeof(Dictionary<string, Cubemap>), (c) => (c as SkyboxBehaviour).skyboxTextures.Keys, null));
        skyboxMappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.CurrentSkybox", typeof(string), (c) => (c as SkyboxBehaviour).currentKey, (c, x) => (c as SkyboxBehaviour).setSkybox((string)x)));
        skyboxMappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.SkyboxTint", typeof(Color), (c) => (c as SkyboxBehaviour).SkyboxTint, (c, x) => (c as SkyboxBehaviour).SkyboxTint = (Color)x));
        skyboxMappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.SkyboxRotation", typeof(float), (c) => (c as SkyboxBehaviour).SkyboxRotation, (c, x) => (c as SkyboxBehaviour).SkyboxRotation = (float)x));
        skyboxMappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.SkyboxExposure", typeof(float), (c) => (c as SkyboxBehaviour).SkyboxExposure, (c, x) => (c as SkyboxBehaviour).SkyboxExposure = (float)x));
        skyboxMappings.Add(new ComponentEntry.FieldMapping("SkyboxBehaviour.AmbientIntensity", typeof(float), c => (c as SkyboxBehaviour).AmbientIntensity, (c, x) => (c as SkyboxBehaviour).AmbientIntensity = (float)x));
        ComponentEntry.AddComponentMappings(typeof(SkyboxBehaviour), "Atmospherics", skyboxMappings);
        mapped = true;
    }

    // Use this for initialization
    void Start() {
        UnityEngine.Object.DontDestroyOnLoad(this.gameObject);
        ModAssetBundle stuff = ModResource.GetAssetBundle("sky");
        skyMaterial = new Material(Shader.Find("Skybox/Cubemap"));
        stuff.AssetBundle.LoadAllAssets<Cubemap>().ToList().ForEach(x => skyboxTextures.Add(x.name, x));
        
        stuff = ModResource.GetAssetBundle("flares");
        lensFlares = new Dictionary<String, Flare>();
        lensFlares.Add("null", null);
        stuff.AssetBundle.LoadAllAssets<Flare>().ToList().ForEach(x => lensFlares.Add(x.name, x));
    }

    public void Update() {
        if (!Camera.main.gameObject.GetComponent<FlareLayer>()) {
            Camera.main.gameObject.AddComponent<FlareLayer>();
        }
    }

    public void setSkybox(string key) {
        if (key.Equals("null")) {
            currentKey = key;
            Camera.main.clearFlags = CameraClearFlags.Color;
            RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
            DynamicGI.UpdateEnvironment();
            return;
        }
        foreach (String s in skyboxTextures.Keys) {
            if (s.StartsWith(key)) {
                currentKey = key;
                Camera.main.clearFlags = CameraClearFlags.Skybox;
                RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;
                skyMaterial.SetTexture("_Tex", skyboxTextures[s]);
                RenderSettings.skybox = skyMaterial;
                DynamicGI.UpdateEnvironment();
                break;
            }
        }
    }

    public Color SkyboxTint {
        get {
            return (RenderSettings.skybox) ? RenderSettings.skybox.GetColor("_Tint") : Color.clear;
        }
        set {
            RenderSettings.skybox?.SetColor("_Tint", value);
        }
    }

    public float SkyboxRotation {
        get {
            return (RenderSettings.skybox) ? RenderSettings.skybox.GetFloat("_Rotation") : 0f;
        }
        set {
            RenderSettings.skybox?.SetFloat("_Rotation", value);
        }
    }

    public float SkyboxExposure {
        get {
            return (RenderSettings.skybox) ? RenderSettings.skybox.GetFloat("_Exposure") : 0f;
        }
        set {
            RenderSettings.skybox.SetFloat("_Exposure", value);
        }
    }

    public float AmbientIntensity {
        get {
            return RenderSettings.ambientIntensity;
        }
        set {
            RenderSettings.ambientIntensity = value;
            DynamicGI.UpdateEnvironment();
        }
    }
}
