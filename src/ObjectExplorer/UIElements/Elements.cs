﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace spaar.ModLoader.UI {
    // Token: 0x0200006C RID: 108
    public static class Elements {
        // Token: 0x17000066 RID: 102
        // (get) Token: 0x06000317 RID: 791 RVA: 0x0000E45C File Offset: 0x0000C65C
        public static Settings Settings {
            get {
                Settings result;
                if ((result = Elements._settings) == null) {
                    result = (Elements._settings = new Settings());
                }
                return result;
            }
        }

        // Token: 0x17000067 RID: 103
        // (get) Token: 0x06000318 RID: 792 RVA: 0x0000E472 File Offset: 0x0000C672
        // (set) Token: 0x06000319 RID: 793 RVA: 0x0000E479 File Offset: 0x0000C679
        public static bool IsInitialized { get; private set; }

        // Token: 0x17000068 RID: 104
        // (get) Token: 0x0600031A RID: 794 RVA: 0x0000E481 File Offset: 0x0000C681
        // (set) Token: 0x0600031B RID: 795 RVA: 0x0000E488 File Offset: 0x0000C688
        public static Colors Colors { get; private set; }

        // Token: 0x17000069 RID: 105
        // (get) Token: 0x0600031C RID: 796 RVA: 0x0000E490 File Offset: 0x0000C690
        // (set) Token: 0x0600031D RID: 797 RVA: 0x0000E497 File Offset: 0x0000C697
        public static Windows Windows { get; private set; }

        // Token: 0x1700006A RID: 106
        // (get) Token: 0x0600031E RID: 798 RVA: 0x0000E49F File Offset: 0x0000C69F
        // (set) Token: 0x0600031F RID: 799 RVA: 0x0000E4A6 File Offset: 0x0000C6A6
        public static Labels Labels { get; private set; }

        // Token: 0x1700006B RID: 107
        // (get) Token: 0x06000320 RID: 800 RVA: 0x0000E4AE File Offset: 0x0000C6AE
        // (set) Token: 0x06000321 RID: 801 RVA: 0x0000E4B5 File Offset: 0x0000C6B5
        public static Buttons Buttons { get; private set; }

        // Token: 0x1700006C RID: 108
        // (get) Token: 0x06000322 RID: 802 RVA: 0x0000E4BD File Offset: 0x0000C6BD
        // (set) Token: 0x06000323 RID: 803 RVA: 0x0000E4C4 File Offset: 0x0000C6C4
        public static Tools Tools { get; private set; }

        // Token: 0x1700006D RID: 109
        // (get) Token: 0x06000324 RID: 804 RVA: 0x0000E4CC File Offset: 0x0000C6CC
        // (set) Token: 0x06000325 RID: 805 RVA: 0x0000E4D3 File Offset: 0x0000C6D3
        public static InputFields InputFields { get; private set; }

        // Token: 0x1700006E RID: 110
        // (get) Token: 0x06000326 RID: 806 RVA: 0x0000E4DB File Offset: 0x0000C6DB
        // (set) Token: 0x06000327 RID: 807 RVA: 0x0000E4E2 File Offset: 0x0000C6E2
        public static Scrollview Scrollview { get; private set; }

        // Token: 0x1700006F RID: 111
        // (get) Token: 0x06000328 RID: 808 RVA: 0x0000E4EA File Offset: 0x0000C6EA
        // (set) Token: 0x06000329 RID: 809 RVA: 0x0000E4F1 File Offset: 0x0000C6F1
        public static Sliders Sliders { get; private set; }

        // Token: 0x17000070 RID: 112
        // (get) Token: 0x0600032A RID: 810 RVA: 0x0000E4F9 File Offset: 0x0000C6F9
        // (set) Token: 0x0600032B RID: 811 RVA: 0x0000E500 File Offset: 0x0000C700
        public static Toggle Toggle { get; private set; }

        // Token: 0x0600032C RID: 812 RVA: 0x0000E508 File Offset: 0x0000C708
        public static void RebuildElements() {
            Elements.IsInitialized = true;
            Elements.Colors = new Colors();
            Elements.Labels = new Labels();
            Elements.Windows = new Windows();
            Elements.Buttons = new Buttons();
            Elements.Tools = new Tools();
            Elements.InputFields = new InputFields();
            Elements.Scrollview = new Scrollview();
            Elements.Sliders = new Sliders();
            Elements.Toggle = new Toggle();
            SingleInstance<ModGUI>.Instance.RebuildSkin();
        }

        // Token: 0x0600032D RID: 813 RVA: 0x0000E580 File Offset: 0x0000C780
        //internal static Texture2D LoadImage(string name) {
        //    if (Elements.loadedTextures.ContainsKey(name)) {
        //        Texture2D texture2D;
        //        Elements.loadedTextures.TryGetValue(name, out texture2D);
        //        if (texture2D != null) {
        //            return texture2D;
        //        }
        //    }
        //    Texture2D result;
        //    try {
        //        byte[] data = File.ReadAllBytes(Application.dataPath + "/Mods/Resources/ModLoader/UI/" + name);
        //        Texture2D texture2D2 = new Texture2D(0, 0);
        //        texture2D2.LoadImage(data);
        //        Elements.loadedTextures.Add(name, texture2D2);
        //        result = texture2D2;
        //    }
        //    catch (Exception exception) {
        //        Debug.LogWarning("Failed to load: " + name);
        //        Debug.LogException(exception);
        //        result = null;
        //    }
        //    return result;
        //}

        // Token: 0x04000187 RID: 391
        private static Settings _settings;

        // Token: 0x04000192 RID: 402
        //private static readonly Dictionary<string, Texture2D> loadedTextures = new Dictionary<string, Texture2D>();
    }
}
