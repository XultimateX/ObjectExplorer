﻿using System;
using UnityEngine;

namespace spaar.ModLoader.UI {
    // Token: 0x02000070 RID: 112
    public class Settings {
        // Token: 0x1700007E RID: 126
        // (get) Token: 0x0600034C RID: 844 RVA: 0x0000E9FB File Offset: 0x0000CBFB
        // (set) Token: 0x0600034D RID: 845 RVA: 0x0000EA03 File Offset: 0x0000CC03
        public RectOffset DefaultMargin { get; set; }

        // Token: 0x1700007F RID: 127
        // (get) Token: 0x0600034E RID: 846 RVA: 0x0000EA0C File Offset: 0x0000CC0C
        // (set) Token: 0x0600034F RID: 847 RVA: 0x0000EA14 File Offset: 0x0000CC14
        public RectOffset DefaultPadding { get; set; }

        // Token: 0x17000080 RID: 128
        // (get) Token: 0x06000350 RID: 848 RVA: 0x0000EA1D File Offset: 0x0000CC1D
        // (set) Token: 0x06000351 RID: 849 RVA: 0x0000EA25 File Offset: 0x0000CC25
        public RectOffset LowMargin { get; set; }

        // Token: 0x17000081 RID: 129
        // (get) Token: 0x06000352 RID: 850 RVA: 0x0000EA2E File Offset: 0x0000CC2E
        // (set) Token: 0x06000353 RID: 851 RVA: 0x0000EA36 File Offset: 0x0000CC36
        public RectOffset LowPadding { get; set; }

        // Token: 0x17000082 RID: 130
        // (get) Token: 0x06000354 RID: 852 RVA: 0x0000EA3F File Offset: 0x0000CC3F
        // (set) Token: 0x06000355 RID: 853 RVA: 0x0000EA47 File Offset: 0x0000CC47
        public float InspectorPanelWidth { get; set; }

        // Token: 0x17000083 RID: 131
        // (get) Token: 0x06000356 RID: 854 RVA: 0x0000EA50 File Offset: 0x0000CC50
        // (set) Token: 0x06000357 RID: 855 RVA: 0x0000EA58 File Offset: 0x0000CC58
        public float HierarchyPanelWidth { get; set; }

        // Token: 0x17000084 RID: 132
        // (get) Token: 0x06000358 RID: 856 RVA: 0x0000EA61 File Offset: 0x0000CC61
        // (set) Token: 0x06000359 RID: 857 RVA: 0x0000EA69 File Offset: 0x0000CC69
        public Vector2 ConsoleSize { get; set; }

        // Token: 0x17000085 RID: 133
        // (get) Token: 0x0600035A RID: 858 RVA: 0x0000EA72 File Offset: 0x0000CC72
        // (set) Token: 0x0600035B RID: 859 RVA: 0x0000EA7A File Offset: 0x0000CC7A
        public float LogEntrySize { get; set; }

        // Token: 0x17000086 RID: 134
        // (get) Token: 0x0600035C RID: 860 RVA: 0x0000EA83 File Offset: 0x0000CC83
        // (set) Token: 0x0600035D RID: 861 RVA: 0x0000EA8B File Offset: 0x0000CC8B
        public float TreeEntryIndention { get; set; }

        // Token: 0x0600035E RID: 862 RVA: 0x0000EA94 File Offset: 0x0000CC94
        internal Settings() {
            this.DefaultMargin = new RectOffset(8, 8, 8, 8);
            this.DefaultPadding = new RectOffset(8, 8, 6, 6);
            this.LowMargin = new RectOffset(4, 4, 4, 4);
            this.LowPadding = new RectOffset(4, 4, 2, 2);
            this.HierarchyPanelWidth = 350f;
            this.InspectorPanelWidth = 450f;
            this.ConsoleSize = new Vector2(550f, 600f);
            this.LogEntrySize = 16f;
            this.TreeEntryIndention = 20f;
        }
    }
}
