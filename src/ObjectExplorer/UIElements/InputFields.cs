﻿using System;
using UnityEngine;
using Modding;

namespace spaar.ModLoader.UI {
    // Token: 0x0200006D RID: 109
    public class InputFields {
        // Token: 0x17000071 RID: 113
        // (get) Token: 0x0600032F RID: 815 RVA: 0x0000E620 File Offset: 0x0000C820
        // (set) Token: 0x06000330 RID: 816 RVA: 0x0000E628 File Offset: 0x0000C828
        public GUIStyle Default { get; set; }

        // Token: 0x17000072 RID: 114
        // (get) Token: 0x06000331 RID: 817 RVA: 0x0000E631 File Offset: 0x0000C831
        // (set) Token: 0x06000332 RID: 818 RVA: 0x0000E639 File Offset: 0x0000C839
        public GUIStyle Alternate { get; set; }

        // Token: 0x17000073 RID: 115
        // (get) Token: 0x06000333 RID: 819 RVA: 0x0000E642 File Offset: 0x0000C842
        // (set) Token: 0x06000334 RID: 820 RVA: 0x0000E64A File Offset: 0x0000C84A
        public GUIStyle AlternateThin { get; set; }

        // Token: 0x17000074 RID: 116
        // (get) Token: 0x06000335 RID: 821 RVA: 0x0000E653 File Offset: 0x0000C853
        // (set) Token: 0x06000336 RID: 822 RVA: 0x0000E65B File Offset: 0x0000C85B
        public GUIStyle ThinNoTopBotMargin { get; set; }

        // Token: 0x17000075 RID: 117
        // (get) Token: 0x06000337 RID: 823 RVA: 0x0000E664 File Offset: 0x0000C864
        // (set) Token: 0x06000338 RID: 824 RVA: 0x0000E66C File Offset: 0x0000C86C
        public GUIStyle ComponentField { get; set; }

        // Token: 0x06000339 RID: 825 RVA: 0x0000E678 File Offset: 0x0000C878
        internal InputFields() {
            this.Default = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("background-dark.png"),
                    textColor = Elements.Colors.DefaultText
                },
                font = GUI.skin.font,
                alignment = TextAnchor.UpperLeft,
                clipping = TextClipping.Clip,
                border = new RectOffset(4, 4, 4, 4),
                padding = Elements.Settings.DefaultPadding,
                margin = Elements.Settings.DefaultMargin,
                fontSize = 14
            };
            this.Alternate = new GUIStyle(this.Default) {
                normal =
                {
                    background = ModResource.GetTexture("button-normal.png"),
                    textColor = Elements.Colors.DefaultText
                }
            };
            RectOffset lowMargin = Elements.Settings.LowMargin;
            lowMargin.left = 0;
            this.ComponentField = new GUIStyle(this.Alternate) {
                margin = lowMargin,
                padding = Elements.Settings.LowPadding,
                fontSize = 13
            };
            this.ThinNoTopBotMargin = new GUIStyle(this.Default) {
                margin = Elements.Buttons.ThinNoTopBotMargin.margin,
                padding = Elements.Buttons.ThinNoTopBotMargin.padding
            };
        }
    }
}
