﻿using System;
using UnityEngine;
using Modding;

namespace spaar.ModLoader.UI {
    // Token: 0x02000072 RID: 114
    public class Toggle {
        // Token: 0x1700008B RID: 139
        // (get) Token: 0x06000368 RID: 872 RVA: 0x0000EC77 File Offset: 0x0000CE77
        // (set) Token: 0x06000369 RID: 873 RVA: 0x0000EC7F File Offset: 0x0000CE7F
        public GUIStyle Default { get; set; }

        // Token: 0x0600036A RID: 874 RVA: 0x0000EC88 File Offset: 0x0000CE88
        internal Toggle() {
            this.Default = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("toggle-normal.png")
                },
                onNormal =
                {
                    background = ModResource.GetTexture("toggle-on-normal.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("toggle-hover.png")
                },
                onHover =
                {
                    background = ModResource.GetTexture("toggle-on-hover.png")
                },
                active =
                {
                    background = ModResource.GetTexture("toggle-active.png")
                },
                onActive =
                {
                    background = ModResource.GetTexture("toggle-on-active.png")
                },
                margin =
                {
                    right = 10
                },
                fixedWidth = 15f
            };
        }
    }
}
