﻿using System;
using UnityEngine;
using Modding;

namespace spaar.ModLoader.UI {
    // Token: 0x02000069 RID: 105
    public class Buttons {
        // Token: 0x17000054 RID: 84
        // (get) Token: 0x060002F0 RID: 752 RVA: 0x0000DF1B File Offset: 0x0000C11B
        // (set) Token: 0x060002F1 RID: 753 RVA: 0x0000DF23 File Offset: 0x0000C123
        public GUIStyle Default { get; set; }

        // Token: 0x17000055 RID: 85
        // (get) Token: 0x060002F2 RID: 754 RVA: 0x0000DF2C File Offset: 0x0000C12C
        // (set) Token: 0x060002F3 RID: 755 RVA: 0x0000DF34 File Offset: 0x0000C134
        public GUIStyle Disabled { get; set; }
        public GUIStyle Enabled { get; set; }

        // Token: 0x17000057 RID: 87
        // (get) Token: 0x060002F6 RID: 758 RVA: 0x0000DF4E File Offset: 0x0000C14E
        // (set) Token: 0x060002F7 RID: 759 RVA: 0x0000DF56 File Offset: 0x0000C156
        public GUIStyle ComponentField { get; set; }

        // Token: 0x17000058 RID: 88
        // (get) Token: 0x060002F8 RID: 760 RVA: 0x0000DF5F File Offset: 0x0000C15F
        // (set) Token: 0x060002F9 RID: 761 RVA: 0x0000DF67 File Offset: 0x0000C167
        public GUIStyle LogEntryLabel { get; set; }

        public GUIStyle LogEntryLabelSelected { get; set; }

        public GUIStyle LogEntryLabelDisabled { get; set; }

        public GUIStyle LogEntryLabelSelectedDisabled { get; set; }

        // Token: 0x17000059 RID: 89
        // (get) Token: 0x060002FA RID: 762 RVA: 0x0000DF70 File Offset: 0x0000C170
        // (set) Token: 0x060002FB RID: 763 RVA: 0x0000DF78 File Offset: 0x0000C178
        public GUIStyle ThinNoTopBotMargin { get; set; }

        // Token: 0x1700005A RID: 90
        // (get) Token: 0x060002FC RID: 764 RVA: 0x0000DF81 File Offset: 0x0000C181
        // (set) Token: 0x060002FD RID: 765 RVA: 0x0000DF89 File Offset: 0x0000C189
        public GUIStyle ArrowExpanded { get; set; }

        // Token: 0x1700005B RID: 91
        // (get) Token: 0x060002FE RID: 766 RVA: 0x0000DF92 File Offset: 0x0000C192
        // (set) Token: 0x060002FF RID: 767 RVA: 0x0000DF9A File Offset: 0x0000C19A
        public GUIStyle ArrowCollapsed { get; set; }

        // Token: 0x1700005C RID: 92
        // (get) Token: 0x06000300 RID: 768 RVA: 0x0000DFA3 File Offset: 0x0000C1A3
        // (set) Token: 0x06000301 RID: 769 RVA: 0x0000DFAB File Offset: 0x0000C1AB
        public GUIStyle ArrowDarkExpanded { get; set; }

        // Token: 0x1700005D RID: 93
        // (get) Token: 0x06000302 RID: 770 RVA: 0x0000DFB4 File Offset: 0x0000C1B4
        // (set) Token: 0x06000303 RID: 771 RVA: 0x0000DFBC File Offset: 0x0000C1BC
        public GUIStyle ArrowDarkCollapsed { get; set; }

        // Token: 0x06000304 RID: 772 RVA: 0x0000DFC8 File Offset: 0x0000C1C8
        internal Buttons() {
            Color defaultText = Elements.Colors.DefaultText;
            this.Default = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("button-dark.png"),
                    textColor = defaultText
                },
                hover =
                {
                    background = ModResource.GetTexture("button-normal.png"),
                    textColor = defaultText
                },
                active =
                {
                    background = ModResource.GetTexture("button-light-enabled.png"),
                    textColor = defaultText
                },
                border = new RectOffset(4, 4, 4, 4),
                padding = Elements.Settings.DefaultPadding,
                margin = Elements.Settings.DefaultMargin,
                alignment = TextAnchor.MiddleCenter,
                fontSize = 14,
                fontStyle = FontStyle.Bold
            };
            this.Enabled = new GUIStyle(this.Default) {
                normal =
                {
                    background = ModResource.GetTexture("button-normal-enabled.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("button-light-enabled.png"),
                    textColor = defaultText
                },
                active =
                {
                    background = ModResource.GetTexture("button-dark-enabled.png"),
                    textColor = defaultText
                }
            };
            this.Disabled = new GUIStyle(this.Default) {
                normal =
                {
                    background = ModResource.GetTexture("button-dark.png")
                }
            };
            RectOffset lowMargin = Elements.Settings.LowMargin;
            lowMargin.left = 0;
            this.ComponentField = new GUIStyle(this.Default) {
                margin = lowMargin,
                padding = Elements.Settings.LowPadding,
                fontSize = 13
            };
            this.LogEntryLabel = new GUIStyle(Elements.Labels.LogEntry) {
                hover =
                {
                    background = ModResource.GetTexture("button-light.png"),
                    textColor = defaultText
                },
                active =
                {
                    background = ModResource.GetTexture("button-dark-enabled.png"),
                }
            };
            this.LogEntryLabelSelected = new GUIStyle(Elements.Labels.LogEntry) {
                normal =
                {
                    background = ModResource.GetTexture("button-normal-enabled.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("button-light-enabled.png"),
                    textColor = Elements.Colors.LogNormal
                },
                active =
                {
                    background = ModResource.GetTexture("button-dark-enabled.png"),
                }
            };
            this.LogEntryLabelDisabled = new GUIStyle(Elements.Labels.LogEntry) {
                normal = {
                    textColor = Elements.Colors.LowlightText
                },
                hover =
                {
                    background = ModResource.GetTexture("button-light.png"),
                    textColor = defaultText
                },
                active =
                {
                    background = ModResource.GetTexture("button-dark-enabled.png"),
                }
            };
            this.LogEntryLabelSelectedDisabled = new GUIStyle(Elements.Labels.LogEntry) {
                normal =
                {
                    background = ModResource.GetTexture("button-dark-enabled.png"),
                    textColor = Elements.Colors.LowlightText
                },
                hover =
                {
                    background = ModResource.GetTexture("button-light-enabled.png"),
                    textColor = defaultText
                },
                active =
                {
                    background = ModResource.GetTexture("button-very-dark-enabled.png"),
                }
            };
            this.ThinNoTopBotMargin = new GUIStyle(this.Default) {
                margin = new RectOffset(this.Default.margin.left, this.Default.margin.right, 0, 0)
            };
            this.ArrowCollapsed = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("arrow-normal-right.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("arrow-hover-right.png")
                },
                active =
                {
                    background = ModResource.GetTexture("arrow-disabled-right.png")
                },
                alignment = TextAnchor.MiddleCenter,
                margin = new RectOffset(0, 6, 2, 2)
            };
            this.ArrowExpanded = new GUIStyle(this.ArrowCollapsed) {
                normal =
                {
                    background = ModResource.GetTexture("arrow-normal-down.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("arrow-hover-down.png")
                },
                active =
                {
                    background = ModResource.GetTexture("arrow-disabled-down.png")
                }
            };
            this.ArrowDarkCollapsed = new GUIStyle(this.ArrowCollapsed) {
                normal =
                {
                    background = ModResource.GetTexture("arrow-disabled-right.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("arrow-disabled-right.png")
                }
            };
            this.ArrowDarkExpanded = new GUIStyle(this.ArrowExpanded) {
                normal =
                {
                    background = ModResource.GetTexture("arrow-disabled-down.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("arrow-disabled-down.png")
                }
            };
        }
    }
}
