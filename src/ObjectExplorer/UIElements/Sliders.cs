﻿using System;
using UnityEngine;
using Modding;

namespace spaar.ModLoader.UI {
    // Token: 0x02000071 RID: 113
    public class Sliders {
        // Token: 0x17000087 RID: 135
        // (get) Token: 0x0600035F RID: 863 RVA: 0x0000EB24 File Offset: 0x0000CD24
        // (set) Token: 0x06000360 RID: 864 RVA: 0x0000EB2C File Offset: 0x0000CD2C
        public GUIStyle Horizontal { get; set; }

        // Token: 0x17000088 RID: 136
        // (get) Token: 0x06000361 RID: 865 RVA: 0x0000EB35 File Offset: 0x0000CD35
        // (set) Token: 0x06000362 RID: 866 RVA: 0x0000EB3D File Offset: 0x0000CD3D
        public GUIStyle Vertical { get; set; }

        // Token: 0x17000089 RID: 137
        // (get) Token: 0x06000363 RID: 867 RVA: 0x0000EB46 File Offset: 0x0000CD46
        // (set) Token: 0x06000364 RID: 868 RVA: 0x0000EB4E File Offset: 0x0000CD4E
        public GUIStyle ThumbHorizontal { get; set; }

        // Token: 0x1700008A RID: 138
        // (get) Token: 0x06000365 RID: 869 RVA: 0x0000EB57 File Offset: 0x0000CD57
        // (set) Token: 0x06000366 RID: 870 RVA: 0x0000EB5F File Offset: 0x0000CD5F
        public GUIStyle ThumbVertical { get; set; }

        // Token: 0x06000367 RID: 871 RVA: 0x0000EB68 File Offset: 0x0000CD68
        internal Sliders() {
            this.Horizontal = new GUIStyle(GUI.skin.horizontalSlider) {
                normal =
                {
                    background = ModResource.GetTexture("button-normal.png")
                }
            };
            this.Vertical = new GUIStyle(GUI.skin.verticalSlider) {
                normal =
                {
                    background = ModResource.GetTexture("button-normal.png")
                }
            };
            this.ThumbHorizontal = new GUIStyle(GUI.skin.horizontalSliderThumb) {
                normal =
                {
                    background = ModResource.GetTexture("slider-thumb.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("slider-thumb-hover.png")
                },
                active =
                {
                    background = ModResource.GetTexture("slider-thumb-active.png")
                }
            };
            this.ThumbVertical = new GUIStyle(GUI.skin.verticalSliderThumb) {
                normal =
                {
                    background = ModResource.GetTexture("slider-thumb.png")
                },
                hover =
                {
                    background = ModResource.GetTexture("slider-thumb-hover.png")
                },
                active =
                {
                    background = ModResource.GetTexture("slider-thumb-active.png")
                }
            };
        }
    }
}
