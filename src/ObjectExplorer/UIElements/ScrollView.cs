﻿using System;
using UnityEngine;
using Modding;

namespace spaar.ModLoader.UI {
    // Token: 0x0200006F RID: 111
    public class Scrollview {
        // Token: 0x1700007A RID: 122
        // (get) Token: 0x06000343 RID: 835 RVA: 0x0000E8BC File Offset: 0x0000CABC
        // (set) Token: 0x06000344 RID: 836 RVA: 0x0000E8C4 File Offset: 0x0000CAC4
        public GUIStyle Horizontal { get; set; }

        // Token: 0x1700007B RID: 123
        // (get) Token: 0x06000345 RID: 837 RVA: 0x0000E8CD File Offset: 0x0000CACD
        // (set) Token: 0x06000346 RID: 838 RVA: 0x0000E8D5 File Offset: 0x0000CAD5
        public GUIStyle Vertical { get; set; }

        // Token: 0x1700007C RID: 124
        // (get) Token: 0x06000347 RID: 839 RVA: 0x0000E8DE File Offset: 0x0000CADE
        // (set) Token: 0x06000348 RID: 840 RVA: 0x0000E8E6 File Offset: 0x0000CAE6
        public GUIStyle ThumbVertical { get; set; }

        // Token: 0x1700007D RID: 125
        // (get) Token: 0x06000349 RID: 841 RVA: 0x0000E8EF File Offset: 0x0000CAEF
        // (set) Token: 0x0600034A RID: 842 RVA: 0x0000E8F7 File Offset: 0x0000CAF7
        public GUIStyle ThumbHorizontal { get; set; }

        // Token: 0x0600034B RID: 843 RVA: 0x0000E900 File Offset: 0x0000CB00
        internal Scrollview() {
            this.Horizontal = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("scroll-horizontal.png")
                },
                fixedHeight = 13f,
                border = new RectOffset(6, 6, 3, 3)
            };
            this.Vertical = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("scroll-vertical.png")
                },
                fixedWidth = 13f,
                border = new RectOffset(3, 3, 6, 6)
            };
            this.ThumbHorizontal = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("thumb-horizontal.png")
                },
                fixedHeight = 13f,
                border = new RectOffset(6, 6, 3, 3)
            };
            this.ThumbVertical = new GUIStyle {
                normal =
                {
                    background = ModResource.GetTexture("thumb-vertical.png")
                },
                fixedWidth = 13f,
                border = new RectOffset(3, 3, 6, 6)
            };
        }
    }
}
