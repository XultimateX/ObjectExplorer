﻿using System;
using spaar.ModLoader.Internal;
using UnityEngine;

namespace spaar.ModLoader.UI {
    // Token: 0x02000074 RID: 116
    public class ModGUI : SingleInstance<ModGUI> {
        // Token: 0x1700008C RID: 140
        // (get) Token: 0x06000370 RID: 880 RVA: 0x0000EDED File Offset: 0x0000CFED
        public override string Name {
            get {
                return "spaar's Mod Loader: GUI Util";
            }
        }

        // Token: 0x1700008D RID: 141
        // (get) Token: 0x06000371 RID: 881 RVA: 0x0000EDF4 File Offset: 0x0000CFF4
        // (set) Token: 0x06000372 RID: 882 RVA: 0x0000EE00 File Offset: 0x0000D000
        public static GUISkin Skin {
            get {
                return SingleInstance<ModGUI>.Instance._skin;
            }
            set {
                SingleInstance<ModGUI>.Instance._skin = value;
            }
        }

        // Token: 0x06000373 RID: 883 RVA: 0x0000EE0D File Offset: 0x0000D00D
        private void Awake() {
            DontDestroyOnLoad(Instance);
        }

        // Token: 0x06000374 RID: 884 RVA: 0x0000EE15 File Offset: 0x0000D015
        private void OnGUI() {
            if (!Elements.IsInitialized) {
                Elements.RebuildElements();
            }
        }

        // Token: 0x06000375 RID: 885 RVA: 0x0000EE24 File Offset: 0x0000D024
        public void RebuildSkin() {
            ModGUI.Skin = ScriptableObject.CreateInstance<GUISkin>();
            ModGUI.Skin.window = Elements.Windows.Default;
            ModGUI.Skin.label = Elements.Labels.Default;
            ModGUI.Skin.button = Elements.Buttons.Default;
            ModGUI.Skin.textField = (ModGUI.Skin.textArea = Elements.InputFields.Default);
            ModGUI.Skin.horizontalScrollbar = Elements.Scrollview.Horizontal;
            ModGUI.Skin.verticalScrollbar = Elements.Scrollview.Vertical;
            ModGUI.Skin.verticalScrollbarThumb = Elements.Scrollview.ThumbVertical;
            ModGUI.Skin.horizontalScrollbar = Elements.Scrollview.Horizontal;
            ModGUI.Skin.horizontalScrollbarThumb = Elements.Scrollview.ThumbHorizontal;
            ModGUI.Skin.scrollView = Elements.Windows.ClearDark;
            ModGUI.Skin.horizontalSlider = Elements.Sliders.Horizontal;
            ModGUI.Skin.horizontalSliderThumb = Elements.Sliders.ThumbHorizontal;
            ModGUI.Skin.verticalSlider = Elements.Sliders.Vertical;
            ModGUI.Skin.verticalSliderThumb = Elements.Sliders.ThumbVertical;
            ModGUI.Skin.toggle = Elements.Toggle.Default;
        }

        // Token: 0x040001AE RID: 430
        private GUISkin _skin;
    }
}
