﻿using System;
using UnityEngine;

namespace spaar.ModLoader.UI {
    // Token: 0x0200006A RID: 106
    public class Colors {
        // Token: 0x1700005E RID: 94
        // (get) Token: 0x06000305 RID: 773 RVA: 0x0000E300 File Offset: 0x0000C500
        // (set) Token: 0x06000306 RID: 774 RVA: 0x0000E308 File Offset: 0x0000C508
        public Color DefaultText { get; set; }

        // Token: 0x1700005F RID: 95
        // (get) Token: 0x06000307 RID: 775 RVA: 0x0000E311 File Offset: 0x0000C511
        // (set) Token: 0x06000308 RID: 776 RVA: 0x0000E319 File Offset: 0x0000C519
        public Color LowlightText { get; set; }

        // Token: 0x17000060 RID: 96
        // (get) Token: 0x06000309 RID: 777 RVA: 0x0000E322 File Offset: 0x0000C522
        // (set) Token: 0x0600030A RID: 778 RVA: 0x0000E32A File Offset: 0x0000C52A
        public Color TypeText { get; set; }

        // Token: 0x17000061 RID: 97
        // (get) Token: 0x0600030B RID: 779 RVA: 0x0000E333 File Offset: 0x0000C533
        // (set) Token: 0x0600030C RID: 780 RVA: 0x0000E33B File Offset: 0x0000C53B
        public Color LogNormal { get; set; }

        // Token: 0x17000062 RID: 98
        // (get) Token: 0x0600030D RID: 781 RVA: 0x0000E344 File Offset: 0x0000C544
        // (set) Token: 0x0600030E RID: 782 RVA: 0x0000E34C File Offset: 0x0000C54C
        public Color LogWarning { get; set; }

        // Token: 0x17000063 RID: 99
        // (get) Token: 0x0600030F RID: 783 RVA: 0x0000E355 File Offset: 0x0000C555
        // (set) Token: 0x06000310 RID: 784 RVA: 0x0000E35D File Offset: 0x0000C55D
        public Color LogError { get; set; }

        // Token: 0x17000064 RID: 100
        // (get) Token: 0x06000311 RID: 785 RVA: 0x0000E366 File Offset: 0x0000C566
        // (set) Token: 0x06000312 RID: 786 RVA: 0x0000E36E File Offset: 0x0000C56E
        public Color LogException { get; set; }

        // Token: 0x17000065 RID: 101
        // (get) Token: 0x06000313 RID: 787 RVA: 0x0000E377 File Offset: 0x0000C577
        // (set) Token: 0x06000314 RID: 788 RVA: 0x0000E37F File Offset: 0x0000C57F
        public Color LogAssert { get; set; }

        // Token: 0x06000315 RID: 789 RVA: 0x0000E388 File Offset: 0x0000C588
        internal Colors() {
            this.DefaultText = Color.white;
            this.LowlightText = ColorUtil.FromRGB255(200f, 200f, 200f);
            this.TypeText = ColorUtil.FromRGB255(78f, 201f, 176f);
            this.LogNormal = this.DefaultText;
            this.LogWarning = ColorUtil.FromRGB255(240f, 76f, 23f);
            this.LogError = ColorUtil.FromRGB255(238f, 78f, 16f);
            this.LogException = this.LogWarning;
            this.LogAssert = ColorUtil.FromRGB255(191f, 88f, 203f);
        }
    }
}
