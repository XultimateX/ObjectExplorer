﻿using System;
using UnityEngine;

namespace spaar.ModLoader.UI {
    // Token: 0x0200006E RID: 110
    public class Labels {
        // Token: 0x17000076 RID: 118
        // (get) Token: 0x0600033A RID: 826 RVA: 0x0000E7CD File Offset: 0x0000C9CD
        // (set) Token: 0x0600033B RID: 827 RVA: 0x0000E7D5 File Offset: 0x0000C9D5
        public GUIStyle Default { get; set; }

        // Token: 0x17000077 RID: 119
        // (get) Token: 0x0600033C RID: 828 RVA: 0x0000E7DE File Offset: 0x0000C9DE
        // (set) Token: 0x0600033D RID: 829 RVA: 0x0000E7E6 File Offset: 0x0000C9E6
        public GUIStyle LogEntry { get; set; }
        public GUIStyle LogEntryDisabled { get; set; }

        // Token: 0x17000078 RID: 120
        // (get) Token: 0x0600033E RID: 830 RVA: 0x0000E7EF File Offset: 0x0000C9EF
        // (set) Token: 0x0600033F RID: 831 RVA: 0x0000E7F7 File Offset: 0x0000C9F7
        public GUIStyle LogEntryTitle { get; set; }

        // Token: 0x17000079 RID: 121
        // (get) Token: 0x06000340 RID: 832 RVA: 0x0000E800 File Offset: 0x0000CA00
        // (set) Token: 0x06000341 RID: 833 RVA: 0x0000E808 File Offset: 0x0000CA08
        public GUIStyle Title { get; set; }

        // Token: 0x06000342 RID: 834 RVA: 0x0000E814 File Offset: 0x0000CA14
        internal Labels() {
            this.Default = new GUIStyle {
                font = GUI.skin.font,
                normal =
                {
                    textColor = Elements.Colors.DefaultText
                }
            };
            this.LogEntry = new GUIStyle(this.Default) {
                margin =
                {
                    top = 2
                },
                alignment = TextAnchor.MiddleLeft,
                richText = true
            };
            this.LogEntryDisabled = new GUIStyle(this.Default) {
                normal = {
                    textColor = Elements.Colors.LowlightText
                },
                margin =
                {
                    top = 2
                },
                alignment = TextAnchor.MiddleLeft,
                richText = true,
            };
            this.Title = new GUIStyle(this.Default) {
                fontStyle = FontStyle.Bold
            };
            this.LogEntryTitle = new GUIStyle(this.LogEntry) {
                fontStyle = this.Title.fontStyle
            };
        }
    }
}
